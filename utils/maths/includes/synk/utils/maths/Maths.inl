#pragma once

namespace synk::utils::maths
{
    template<typename T>
    auto dist(const T &fromIn, const T &toIn) noexcept
    {
        auto diff = (toIn - fromIn);
        return diff * diff;
    }

    template<typename T>
    auto dist(const T &fromIn) noexcept
    {
        return fromIn * fromIn;
    }


    // Vec2 distance

    template<typename T>
    auto dist(const Vec2<T> &fromIn, const Vec2<T> &toIn) noexcept
    {
        auto x_diff = toIn.getX() - fromIn.getX();
        auto y_diff = toIn.getY() - fromIn.getY();
        return x_diff * x_diff + y_diff * y_diff;
    }

    template<typename T>
    auto dist(const Vec2<T> &fromIn) noexcept
    {
        return fromIn.getX() * fromIn.getX() + fromIn.getY() * fromIn.getY();
    }


    // Vec3 distance

    template<typename T>
    auto dist(const Vec3<T> &fromIn, const Vec3<T> &toIn) noexcept
    {
        auto x_diff = toIn.getX() - fromIn.getX();
        auto y_diff = toIn.getY() - fromIn.getY();
        auto z_diff = toIn.getZ() - fromIn.getZ();
        return x_diff * x_diff + y_diff * y_diff + z_diff * z_diff;
    }

    template<typename T>
    auto dist(const Vec3<T> &fromIn) noexcept
    {
        return fromIn.getX() * fromIn.getX() + fromIn.getY() * fromIn.getY() + fromIn.getZ() * fromIn.getZ();
    }


    // Vec4 distance

    template<typename T>
    auto dist(const Vec4<T> &fromIn, const Vec4<T> &toIn) noexcept
    {
        auto x_diff = toIn.getX() - fromIn.getX();
        auto y_diff = toIn.getY() - fromIn.getY();
        auto z_diff = toIn.getZ() - fromIn.getZ();
        auto w_diff = toIn.getW() - fromIn.getW();
        return x_diff * x_diff + y_diff * y_diff + z_diff * z_diff + w_diff * w_diff;
    }

    template<typename T>
    auto dist(const Vec4<T> &fromIn) noexcept
    {
        return fromIn.getX() * fromIn.getX() + fromIn.getY() * fromIn.getY() + fromIn.getZ() * fromIn.getZ() +
               fromIn.getW() * fromIn.getW();
    }


    // Vec5 distance

    template<typename T>
    auto dist(const Vec5<T> &fromIn, const Vec5<T> &toIn) noexcept
    {
        auto x_diff = toIn.getX() - fromIn.getX();
        auto y_diff = toIn.getY() - fromIn.getY();
        auto z_diff = toIn.getZ() - fromIn.getZ();
        auto w_diff = toIn.getW() - fromIn.getW();
        auto a_diff = toIn.getA() - fromIn.getA();
        return x_diff * x_diff + y_diff * y_diff + z_diff * z_diff + w_diff * w_diff + a_diff * a_diff;
    }

    template<typename T>
    auto dist(const Vec5<T> &fromIn) noexcept
    {
        return fromIn.getX() * fromIn.getX() + fromIn.getY() * fromIn.getY() + fromIn.getZ() * fromIn.getZ() +
               fromIn.getW() * fromIn.getW() + fromIn.getA() * fromIn.getA();
    }


    // Vec6 distance

    template<typename T>
    auto dist(const Vec6<T> &fromIn, const Vec6<T> &toIn) noexcept
    {
        auto x_diff = toIn.getX() - fromIn.getX();
        auto y_diff = toIn.getY() - fromIn.getY();
        auto z_diff = toIn.getZ() - fromIn.getZ();
        auto w_diff = toIn.getW() - fromIn.getW();
        auto a_diff = toIn.getA() - fromIn.getA();
        auto b_diff = toIn.getB() - fromIn.getB();
        return x_diff * x_diff + y_diff * y_diff + z_diff * z_diff + w_diff * w_diff + a_diff * a_diff + b_diff * b_diff;
    }

    template<typename T>
    auto dist(const Vec6<T> &fromIn) noexcept
    {
        return fromIn.getX() * fromIn.getX() + fromIn.getY() * fromIn.getY() + fromIn.getZ() * fromIn.getZ() +
               fromIn.getW() * fromIn.getW() + fromIn.getA() * fromIn.getA() + fromIn.getB() * fromIn.getB();
    }


    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec6<T>& vecIn)
    {
        out << vecIn.toString();
        return out;
    }

    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec5<T>& vecIn)
    {
        out << vecIn.toString();
        return out;
    }

    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec4<T>& vecIn)
    {
        out << vecIn.toString();
        return out;
    }

    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec3<T>& vecIn)
    {
        out << vecIn.toString();
        return out;
    }

    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec2<T>& vecIn)
    {
        out << vecIn.toString();
        return out;
    }

    template<typename T>
    auto addVec6toVector(std::vector<T> &vectorIn, const Vec6<T> &vecIn) noexcept
    {
        vectorIn.push_back(vecIn.getX());
        vectorIn.push_back(vecIn.getY());
        vectorIn.push_back(vecIn.getZ());
        vectorIn.push_back(vecIn.getW());
        vectorIn.push_back(vecIn.getA());
        vectorIn.push_back(vecIn.getB());

        return vecIn;
    }

    template<typename T, std::size_t Y>
    auto addVec6ToArray(const std::array<T, Y> &arrIn, const Vec6<T> &vecIn) noexcept
    {
        std::array<T, Y + 6> arr;
        std::uint32_t i = 0;
        for(const T& t : arrIn)
        {
            arr[i] = t;
            i ++;
        }

        arr[i] = vecIn.getX();
        arr[i++] = vecIn.getY();
        arr[i++] = vecIn.getZ();
        arr[i++] = vecIn.getW();
        arr[i++] = vecIn.getA();
        arr[i++] = vecIn.getB();

        return arr;
    }
}
