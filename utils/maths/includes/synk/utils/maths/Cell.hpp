#pragma once

#include <iostream>
#include <vector>
#include <functional>

#include <synk/utils/maths/vectors/Vec3.hpp>

namespace synk::utils::maths
{
    template<typename T>
    struct State
    {
    public:
        State(const T &stateIn)
        {
            this->state = stateIn;
        }

        inline auto& setState(T &stateIn) const noexcept
        {
          this->state = stateIn;

          return *this;
        };

        [[nodiscard]] auto& getState() const noexcept { return this->state; }

    private:
        T state;
    };

    template<typename T>
    struct Event
    {
        State<T> state;
        Vec3<float> position;
    };

    template<typename T>
    class Cell
    {
    public:

        Cell(std::function<bool(const Event<T> &eventIn)> lambdaIn)
        {
            this->lambda = lambdaIn;
        }
        inline bool react(const Event<T> &eventIn) noexcept
        {
            return this->lambda(eventIn);
        }

        bool react(const std::vector<Event<T>> &eventsIn, const std::function<bool> &lambdaIn) noexcept;

    private:
        std::function<bool(const Event<T> &eventIn)> lambda;
    };
}
