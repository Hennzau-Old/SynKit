
/*
 * Vec6.inl
 *
 *  Created on: 5 juin 2020
 *      Author: seynax
 */
#pragma once

#include <synk/utils/maths/vectors/Vec2.hpp>
#include <synk/utils/maths/vectors/Vec3.hpp>
#include <synk/utils/maths/vectors/Vec4.hpp>
#include <synk/utils/maths/vectors/Vec5.hpp>
#include <synk/utils/maths/vectors/Vec6.hpp>

namespace synk::utils::maths
{
    template<typename T>
    Vec5<T>::Vec5(const T &xIn, const T &yIn, const T &zIn,
            const T &wIn, const T &aIn)
        : x { static_cast<T> ( xIn ) },
          y { static_cast<T> ( yIn ) },
          z { static_cast<T> ( zIn ) },
          w { static_cast<T> ( wIn ) },
          a { static_cast<T> ( aIn ) }
    {
    }

    template<typename T>
        Vec5<T>::Vec5(const Vec2<T> &vecIn)
        :   x { static_cast<T> ( vecIn.getX() ) },
            y { static_cast<T> ( vecIn.getY() ) }
        {
        }

        template<typename T>
        Vec5<T>::Vec5(const Vec3<T> &vecIn)
        :   x { static_cast<T> ( vecIn.getX() ) },
            y { static_cast<T> ( vecIn.getY() ) },
            z { static_cast<T> ( vecIn.getZ() ) }
        {
        }

        template<typename T>
        Vec5<T>::Vec5(const Vec4<T> &vecIn)
        :   x { static_cast<T> ( vecIn.getX() ) },
            y { static_cast<T> ( vecIn.getY() ) },
            z { static_cast<T> ( vecIn.getZ() ) },
            w { static_cast<T> ( vecIn.getW() ) }
        {
        }

        template<typename T>
        Vec5<T>::Vec5(const Vec5<T> &vecIn)
        :   x { static_cast<T> ( vecIn.getX() ) },
            y { static_cast<T> ( vecIn.getY() ) },
            z { static_cast<T> ( vecIn.getZ() ) },
            w { static_cast<T> ( vecIn.getW() ) },
            a { static_cast<T> ( vecIn.getA() ) }
        {
        }

        template<typename T>
        Vec5<T>::Vec5(const Vec6<T> &vecIn)
        :   x { static_cast<T> ( vecIn.getX() ) },
            y { static_cast<T> ( vecIn.getY() ) },
            z { static_cast<T> ( vecIn.getZ() ) },
            w { static_cast<T> ( vecIn.getW() ) },
            a { static_cast<T> ( vecIn.getA() ) }
        {
        }


    template<typename T>
    Vec5<T>::~Vec5()
    {

    }


    // Add

    template<typename T>
    inline auto& Vec5<T>::add(const T &valueIn) noexcept
    {
        this->x += valueIn;
        this->y += valueIn;
        this->z += valueIn;
        this->w += valueIn;
        this->a += valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::addX(const T &valueIn) noexcept
    {
        this->x += valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::addY(const T &valueIn) noexcept
    {
        this->y += valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::addZ(const T &valueIn) noexcept
    {
        this->z += valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::addW(const T &valueIn) noexcept
    {
        this->w += valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::addA(const T &valueIn) noexcept
    {
        this->a += valueIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::add(const T &xIn, const T &yIn,
                           const T &zIn, const T &wIn,
                           const T &aIn) noexcept
    {
        this->x += xIn;
        this->y += yIn;
        this->z += zIn;
        this->w += wIn;
        this->a += aIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::add(const Vec2<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::add(const Vec3<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        this->z += vecIn.getZ();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::add(const Vec4<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        this->z += vecIn.getZ();
        this->w += vecIn.getW();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::add(const Vec5<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        this->z += vecIn.getZ();
        this->w += vecIn.getW();
        this->a += vecIn.getA();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::add(const Vec6<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        this->z += vecIn.getZ();
        this->w += vecIn.getW();
        this->a += vecIn.getA();
        return *this;
    }


    // Subtract

    template<typename T>
    inline auto& Vec5<T>::sub(const T &valueIn) noexcept
    {
        this->x -= valueIn;
        this->y -= valueIn;
        this->z -= valueIn;
        this->w -= valueIn;
        this->a -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::subX(const T &valueIn) noexcept
    {
        this->x -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::subY(const T &valueIn) noexcept
    {
        this->y -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::subZ(const T &valueIn) noexcept
    {
        this->z -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::subW(const T &valueIn) noexcept
    {
        this->w -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::subA(const T &valueIn) noexcept
    {
        this->a -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::sub(const T &xIn, const T &yIn,
                           const T &zIn, const T &wIn,
                           const T &aIn) noexcept
    {
        this->x -= xIn;
        this->y -= yIn;
        this->z -= zIn;
        this->w -= wIn;
        this->a -= aIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::sub(const Vec2<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::sub(const Vec3<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        this->z -= vecIn.getZ();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::sub(const Vec4<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        this->z -= vecIn.getZ();
        this->w -= vecIn.getW();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::sub(const Vec5<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        this->z -= vecIn.getZ();
        this->w -= vecIn.getW();
        this->a -= vecIn.getA();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::sub(const Vec6<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        this->z -= vecIn.getZ();
        this->w -= vecIn.getW();
        this->a -= vecIn.getA();
        return *this;
    }


    // Multiply

    template<typename T>
    inline auto& Vec5<T>::mul(const T &valueIn) noexcept
    {
        this->x *= valueIn;
        this->y *= valueIn;
        this->z *= valueIn;
        this->w *= valueIn;
        this->a *= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mulX(const T &valueIn) noexcept
    {
        this->x *= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mulY(const T &valueIn) noexcept
    {
        this->y *= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mulZ(const T &valueIn) noexcept
    {
        this->z *= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mulW(const T &valueIn) noexcept
    {
        this->w *= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mulA(const T &valueIn) noexcept
    {
        this->a *= valueIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::mul(const T &xIn, const T &yIn,
                           const T &zIn, const T &wIn,
                           const T &aIn) noexcept
    {
        this->x *= xIn;
        this->y *= yIn;
        this->z *= zIn;
        this->w *= wIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::mul(const Vec2<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mul(const Vec3<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        this->z *= vecIn.getZ();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mul(const Vec4<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        this->z *= vecIn.getZ();
        this->w *= vecIn.getW();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mul(const Vec5<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        this->z *= vecIn.getZ();
        this->w *= vecIn.getW();
        this->a *= vecIn.getA();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::mul(const Vec6<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        this->z *= vecIn.getZ();
        this->w *= vecIn.getW();
        this->a *= vecIn.getA();
        return *this;
    }


    // Divide

    template<typename T>
    inline auto& Vec5<T>::div(const T &valueIn) noexcept
    {
        this->x /= valueIn;
        this->y /= valueIn;
        this->z /= valueIn;
        this->w /= valueIn;
        this->a /= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::divX(const T &valueIn) noexcept
    {
        this->x /= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::divY(const T &valueIn) noexcept
    {
        this->y /= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::divZ(const T &valueIn) noexcept
    {
        this->z /= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::divW(const T &valueIn) noexcept
    {
        this->w /= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::divA(const T &valueIn) noexcept
    {
        this->a /= valueIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::div(const T &xIn, const T &yIn,
                           const T &zIn, const T &wIn,
                           const T &aIn) noexcept
    {
        this->x /= xIn;
        this->y /= yIn;
        this->z /= zIn;
        this->w /= wIn;
        this->a /= aIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::div(const Vec2<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::div(const Vec3<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        this->z /= vecIn.getZ();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::div(const Vec4<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        this->z /= vecIn.getZ();
        this->w /= vecIn.getW();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::div(const Vec5<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        this->z /= vecIn.getZ();
        this->w /= vecIn.getW();
        this->a /= vecIn.getA();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::div(const Vec6<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        this->z /= vecIn.getZ();
        this->w /= vecIn.getW();
        this->a /= vecIn.getA();
        return *this;
    }


    // Dot (ScalarProduct)

    template<typename T>
    inline auto Vec5<T>::dot(const T &valueIn) const noexcept
    {
        return this->x * valueIn + this->y * valueIn + this->z * valueIn +
               this->w * valueIn + this->a * valueIn;
    }

    template<typename T>
    inline auto Vec5<T>::dot(const T &xIn, const T &yIn) const noexcept
    {
        return this->x * xIn + this->y * yIn;
    }

    template<typename T>
    inline auto Vec5<T>::dot(const T &xIn, const T &yIn, const T &zIn) const noexcept
    {
        return this->x * xIn + this->y * yIn + this->z * zIn;
    }

    template<typename T>
    inline auto Vec5<T>::dot(const T &xIn, const T &yIn, const T &zIn,
                          const T &wIn) const noexcept
    {
        return this->x * xIn + this->y * yIn + this->z * zIn + this->w * wIn;
    }

    template<typename T>
    inline auto Vec5<T>::dot(const T &xIn, const T &yIn, const T &zIn,
                          const T &wIn, const T &aIn) const noexcept
    {
        return this->x * xIn + this->y * yIn + this->z * zIn + this->w * wIn + this->a * aIn;
    }


    template<typename T>
    inline auto Vec5<T>::dot(const Vec2<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY();
    }

    template<typename T>
    inline auto Vec5<T>::dot(const Vec3<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY() + this->z * vecIn.getZ();
    }

    template<typename T>
    inline auto Vec5<T>::dot(const Vec4<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY() + this->z * vecIn.getZ() +
               this->w * vecIn.getW();
    }

    template<typename T>
    inline auto Vec5<T>::dot(const Vec5<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY() + this->z * vecIn.getZ() +
               this->w * vecIn.getW() + this->a * vecIn.getA();
    }

    template<typename T>
    inline auto Vec5<T>::dot(const Vec6<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY() + this->z * vecIn.getZ() +
               this->w * vecIn.getW() + this->a * vecIn.getA();
    }


    // Setter

    template<typename T>
    inline auto& Vec5<T>::setX(const T &xIn) noexcept
    {
        this->x = xIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::setY(const T &yIn) noexcept
    {
        this->y = yIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::setZ(const T &zIn) noexcept
    {
        this->z = zIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::setW(const T &wIn) noexcept
    {
        this->w = wIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::setA(const T &aIn) noexcept
    {
        this->a = aIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::set(const T &xIn, const T &yIn, const T &zIn,
                           const T &wIn, const T &aIn) noexcept
    {
        this->x = xIn;
        this->y = yIn;
        this->z = zIn;
        this->w = wIn;
        this->a = aIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec5<T>::set(const Vec2<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::set(const Vec3<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        this->z = vecIn.getZ();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::set(const Vec4<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        this->z = vecIn.getZ();
        this->w = vecIn.getW();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::set(const Vec5<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        this->z = vecIn.getZ();
        this->w = vecIn.getW();
        this->a = vecIn.getA();
        return *this;
    }

    template<typename T>
    inline auto& Vec5<T>::set(const Vec6<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        this->z = vecIn.getZ();
        this->w = vecIn.getW();
        this->a = vecIn.getA();
        return *this;
    }


    // toString

    template<typename T>
    [[nodiscard]] inline std::string Vec5<T>::toString()  const noexcept
    {
        return "[X: " + std::to_string(this->x) + ", Y: " + std::to_string(this->y) + ", Z: " +
                        std::to_string(this->z) + ", W: " + std::to_string(this->w) + ", A: " +
                        std::to_string(this->a) + "]";
    }
}
