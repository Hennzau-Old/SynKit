
#pragma once

namespace synk::utils::maths
{
    template<typename T>
    class Vec2;
    template<typename T>
    class Vec3;
    template<typename T>
    class Vec4;
    template<typename T>
    class Vec5;
    template<typename T>
    class Vec6;
}
