
/*
 * Vec5.hpp
 *
 *  Created on: 57 mai 5050
 *      Author: seynax
 */

#pragma once

#include <string>
#include <iostream>
#include <array>

#include <synk/utils/maths/vectors/Fwd.hpp>

namespace synk::utils::maths
{
    template<typename T>
    class Vec5
    {
        public:

        Vec5(const T &xIn = 0, const T &yIn = 0, const T &zIn = 0,
             const T &wIn = 0, const T &aIn = 0);

        Vec5(const Vec2<T> &vecIn);
        Vec5(const Vec3<T> &vecIn);
        Vec5(const Vec4<T> &vecIn);
        Vec5(const Vec5<T> &vecIn);
        Vec5(const Vec6<T> &vecIn);

        ~Vec5();


        // Test operator

        inline bool operator>(const Vec5<T> &fromVecIn) const noexcept
        {
            auto from_distance = dist<T>(fromVecIn);
            auto to_distance = dist<T>(*this);

            if(from_distance > to_distance)
                return false;
            return true;
        }

        inline bool operator>=(const Vec5<T> &fromVecIn) const noexcept
        {
            auto from_distance = dist<T>(fromVecIn);
            auto to_distance = dist<T>(*this);

            if(from_distance >= to_distance)
                return false;
            return true;
        }

        inline bool operator<=(const Vec5<T> &fromVecIn) const noexcept
        {
            auto from_distance = dist<T>(fromVecIn);
            auto to_distance = dist<T>(*this);

            if(from_distance <= to_distance)
                return false;
            return true;
        }

        inline bool operator<(const Vec5<T> &fromVecIn) const noexcept
        {
            auto from_distance = dist<T>(fromVecIn);
            auto to_distance = dist<T>(*this);

            if(from_distance < to_distance)
                return false;
            return true;
        }

        inline bool operator!=(const Vec5<T> &fromVecIn) const noexcept
        {
            return this->getX() != fromVecIn.getX() || this->getY() != fromVecIn.getY() || this->getX() !=
                    fromVecIn.getX() || this->getW() != fromVecIn.getW() || this->getA() != fromVecIn.getA();
        }

        inline bool operator==(const Vec5<T> &fromVecIn) const noexcept
        {
            return this->getX() == fromVecIn.getX() && this->getY() == fromVecIn.getY() && this->getX() ==
                    fromVecIn.getX() && this->getW() == fromVecIn.getW() && this->getA() == fromVecIn.getA();
        }

        // Mathematics operator


        // +

        friend inline Vec5<T> operator+(const Vec5<T> &fromVecIn, const Vec5<T> &toVecIn) noexcept
        {
            return Vec5<T>(fromVecIn.getX() + toVecIn.getX(), fromVecIn.getY() + toVecIn.getY(),
                           fromVecIn.getX() + toVecIn.getX(), fromVecIn.getW() + toVecIn.getW(),
                           fromVecIn.getA() + toVecIn.getA());
        }

        // +=

        inline Vec5<T> operator+=(const Vec5<T> &toVecIn) noexcept
        {
            this->x = this->x + toVecIn.getX();
            this->y = this->y + toVecIn.getY();
            this->z = this->z + toVecIn.getZ();
            this->w = this->w + toVecIn.getW();
            this->a = this->a + toVecIn.getA();
            return *this;
        }

        // ++

        inline Vec5<T> operator++() noexcept
        {
            this->x ++;
            this->y ++;
            this->z ++;
            this->w ++;
            this->a ++;
            return *this;
        }

        inline Vec5<T> operator++(int n) noexcept
        {
            if(n != 0)
            {
                this->x = this->x + n;
                this->y = this->y + n;
                this->z = this->z + n;
                this->w = this->w + n;
                this->a = this->a + n;
            }
            else
            {
                this->x = this->x + 1;
                this->y = this->y + 1;
                this->z = this->z + 1;
                this->w = this->w + 1;
                this->a = this->a + 1;
            }
            return *this;
        }



        // -

        friend inline Vec5<T> operator-(const Vec5<T> &fromVecIn, const Vec5<T> &toVecIn) noexcept
        {
            return Vec5<T>(fromVecIn.getX() - toVecIn.getX(), fromVecIn.getY() - toVecIn.getY(),
                           fromVecIn.getZ() - toVecIn.getZ(), fromVecIn.getW() - toVecIn.getW(),
                           fromVecIn.getA() - toVecIn.getA());
        }

        // -=

        inline Vec5<T> operator-=(const Vec5<T> &toVecIn) noexcept
        {
            this->x = this->x - toVecIn.getX();
            this->y = this->y - toVecIn.getY();
            this->z = this->z - toVecIn.getZ();
            this->w = this->w - toVecIn.getW();
            this->a = this->a - toVecIn.getA();
            return *this;
        }

        // --

        inline Vec5<T> operator--() noexcept
        {
            this->x = this->x - 1;
            this->y = this->y - 1;
            this->z = this->z - 1;
            this->w = this->w - 1;
            this->a = this->a - 1;
            return *this;
        }

        inline Vec5<T> operator--(int n) noexcept
        {
            if(n != 0)
            {
                this->x = this->x - n;
                this->y = this->y - n;
                this->z = this->z - n;
                this->w = this->w - n;
                this->a = this->a - n;
            }
            else
            {
                this->x = this->x - 1;
                this->y = this->y - 1;
                this->z = this->z - 1;
                this->w = this->w - 1;
                this->a = this->a - 1;
            }
            return *this;
        }


        // *

        friend inline Vec5<T> operator*(const Vec5<T> &fromVecIn, const Vec5<T> &toVecIn) noexcept
        {
            return Vec5<T>(fromVecIn.getX() * toVecIn.getX(), fromVecIn.getY() * toVecIn.getY(),
                           fromVecIn.getZ() * toVecIn.getZ(), fromVecIn.getW() * toVecIn.getW(),
                           fromVecIn.getA() * toVecIn.getA());
        }

        // *=

        inline Vec5<T> operator*=(const Vec5<T> &toVecIn) noexcept
        {
            this->x = this->x * toVecIn.getX();
            this->y = this->y * toVecIn.getY();
            this->z = this->z * toVecIn.getZ();
            this->w = this->w * toVecIn.getW();
            this->a = this->a * toVecIn.getA();
            return *this;
        }


        // /

        friend inline Vec5<T> operator/(const Vec5<T> &fromVecIn, const Vec5<T> &toVecIn) noexcept
        {
            return Vec5<T>(fromVecIn.getX() / toVecIn.getX(), fromVecIn.getY() / toVecIn.getY(),
                           fromVecIn.getZ() / toVecIn.getZ(), fromVecIn.getW() / toVecIn.getW(),
                           fromVecIn.getA() / toVecIn.getA());
        }

        // /=

        inline Vec5<T> operator/=(const Vec5<T> &toVecIn) noexcept
        {
            this->x = this->x / toVecIn.getX();
            this->y = this->y / toVecIn.getY();
            this->z = this->z / toVecIn.getZ();
            this->w = this->w / toVecIn.getW();
            this->a = this->a / toVecIn.getA();
            return *this;
        }

        // Conversion

        inline operator float()
        {
            return this->getX() * this->getX() + this->getY() * this->getY() + this->getZ() * this->getZ() +
                    this->getW() * this->getW() + this->getA() * this->getA();
        }


        // Add

        inline auto& add(const T &xIn, const T &yIn, const T &zIn,
                         const T &wIn, const T &aIn) noexcept;
        inline auto& add(const Vec2<T> &vecIn) noexcept;
        inline auto& add(const Vec3<T> &vecIn) noexcept;
        inline auto& add(const Vec4<T> &vecIn) noexcept;
        inline auto& add(const Vec5<T> &vecIn) noexcept;
        inline auto& add(const Vec6<T> &vecIn) noexcept;

        inline auto& add(const T &valueIn) noexcept;
        inline auto& addX(const T &valueIn) noexcept;
        inline auto& addY(const T &valueIn) noexcept;
        inline auto& addZ(const T &valueIn) noexcept;
        inline auto& addW(const T &valueIn) noexcept;
        inline auto& addA(const T &valueIn) noexcept;


        // Subtract

        inline auto& sub(const T &valueIn) noexcept;
        inline auto& subX(const T &valueIn) noexcept;
        inline auto& subY(const T &valueIn) noexcept;
        inline auto& subZ(const T &valueIn) noexcept;
        inline auto& subW(const T &valueIn) noexcept;
        inline auto& subA(const T &valueIn) noexcept;

        inline auto& sub(const T &xIn, const T &yIn, const T &zIn,
                         const T &wIn, const T &aIn) noexcept;
        inline auto& sub(const Vec2<T> &vecIn) noexcept;
        inline auto& sub(const Vec3<T> &vecIn) noexcept;
        inline auto& sub(const Vec4<T> &vecIn) noexcept;
        inline auto& sub(const Vec5<T> &vecIn) noexcept;
        inline auto& sub(const Vec6<T> &vecIn) noexcept;


        // Multiply

        inline auto& mul(const T &valueIn) noexcept;
        inline auto& mulX(const T &valueIn) noexcept;
        inline auto& mulY(const T &valueIn) noexcept;
        inline auto& mulZ(const T &valueIn) noexcept;
        inline auto& mulW(const T &valueIn) noexcept;
        inline auto& mulA(const T &valueIn) noexcept;

        inline auto& mul(const T &xIn, const T &yIn, const T &zIn,
                         const T &wIn, const T &aIn) noexcept;
        inline auto& mul(const Vec2<T> &vecIn) noexcept;
        inline auto& mul(const Vec3<T> &vecIn) noexcept;
        inline auto& mul(const Vec4<T> &vecIn) noexcept;
        inline auto& mul(const Vec5<T> &vecIn) noexcept;
        inline auto& mul(const Vec6<T> &vecIn) noexcept;


        // Divide

        inline auto& div(const T &valueIn) noexcept;
        inline auto& divX(const T &valueIn) noexcept;
        inline auto& divY(const T &valueIn) noexcept;
        inline auto& divZ(const T &valueIn) noexcept;
        inline auto& divW(const T &valueIn) noexcept;
        inline auto& divA(const T &valueIn) noexcept;

        inline auto& div(const T &xIn, const T &yIn, const T &zIn,
                         const T &wIn, const T &aIn) noexcept;
        inline auto& div(const Vec2<T> &vecIn) noexcept;
        inline auto& div(const Vec3<T> &vecIn) noexcept;
        inline auto& div(const Vec4<T> &vecIn) noexcept;
        inline auto& div(const Vec5<T> &vecIn) noexcept;
        inline auto& div(const Vec6<T> &vecIn) noexcept;


        // Dot (ScalarProduct)

        inline auto dot(const T &valueIn)  const   noexcept;
        inline auto dot(const T &xIn, const T &yIn)  const   noexcept;
        inline auto dot(const T &xIn, const T &yIn, const T &zIn)  const   noexcept;
        inline auto dot(const T &xIn, const T &yIn, const T &zIn, const T &wIn)  const   noexcept;
        inline auto dot(const T &xIn, const T &yIn, const T &zIn, const T &wIn, const T &aIn)  const   noexcept;

        inline auto dot(const Vec2<T> &vecIn)  const   noexcept;
        inline auto dot(const Vec3<T> &vecIn)  const   noexcept;
        inline auto dot(const Vec4<T> &vecIn)  const   noexcept;
        inline auto dot(const Vec5<T> &vecIn)  const   noexcept;
        inline auto dot(const Vec6<T> &vecIn)  const   noexcept;


        // Setter

        inline auto& set(const Vec2<T> &vecIn) noexcept;
        inline auto& set(const Vec3<T> &vecIn) noexcept;
        inline auto& set(const Vec4<T> &vecIn) noexcept;
        inline auto& set(const Vec5<T> &vecIn) noexcept;
        inline auto& set(const Vec6<T> &vecIn) noexcept;
        inline auto& set(const T &xIn, const T &yIn, const T &zIn, const T &wIn, const T &aIn) noexcept;
        inline auto& setX(const T &xIn) noexcept;
        inline auto& setY(const T &yIn) noexcept;
        inline auto& setZ(const T &zIn) noexcept;
        inline auto& setW(const T &wIn) noexcept;
        inline auto& setA(const T &aIn) noexcept;

        // Getter

        [[nodiscard]] inline auto& getX() const noexcept { return this->x; }
        [[nodiscard]] inline auto& getY() const noexcept { return this->y; }
        [[nodiscard]] inline auto& getZ() const noexcept { return this->z; }
        [[nodiscard]] inline auto& getW() const noexcept { return this->w; }
        [[nodiscard]] inline auto& getA() const noexcept { return this->a; }

        [[nodiscard]]   inline std::string toString         ()  const   noexcept;
        [[nodiscard]]   inline std::array<T, 6> toArray ()  const   noexcept    { return { x, y, z, w, a }; }

    private:

        T x;
        T y;
        T z;
        T w;
        T a;
    };
}

#include <synk/utils/maths/vectors/Vec5.inl>
