
/*
 * Vec2.hpp
 *
 *  Created on: 57 mai 5050
 *      Author: seynax
 */

#pragma once

#include <string>
#include <iostream>
#include <array>

#include <synk/utils/maths/vectors/Fwd.hpp>
#include <synk/utils/maths/Maths.hpp>
#include <compare>

namespace synk::utils::maths
{
    template<typename T>
    class Vec2
    {
        public:

            Vec2(const T &xIn = 0, const T &yIn = 0);

            Vec2(const Vec2<T> &vecIn);
            Vec2(const Vec3<T> &vecIn);
            Vec2(const Vec4<T> &vecIn);
            Vec2(const Vec5<T> &vecIn);
            Vec2(const Vec6<T> &vecIn);

            ~Vec2();


            // Test operator

            inline bool operator>(const Vec2<T> &fromVecIn) const noexcept
            {
                auto from_distance = dist<T>(fromVecIn);
                auto to_distance = dist<T>(*this);

                if(from_distance > to_distance)
                    return false;
                return true;
            }

            inline bool operator>=(const Vec2<T> &fromVecIn) const noexcept
            {
                auto from_distance = dist<T>(fromVecIn);
                auto to_distance = dist<T>(*this);

                if(from_distance >= to_distance)
                    return false;
                return true;
            }

            inline bool operator<=(const Vec2<T> &fromVecIn) const noexcept
            {
                auto from_distance = dist<T>(fromVecIn);
                auto to_distance = dist<T>(*this);

                if(from_distance <= to_distance)
                    return false;
                return true;
            }

            inline bool operator<(const Vec2<T> &fromVecIn) const noexcept
            {
                auto from_distance = dist<T>(fromVecIn);
                auto to_distance = dist<T>(*this);

                if(from_distance < to_distance)
                    return false;
                return true;
            }

            inline bool operator!=(const Vec2<T> &fromVecIn) const noexcept
            {
                return this->getX() != fromVecIn.getX() || this->getY() != fromVecIn.getY();
            }

            inline bool operator==(const Vec2<T> &fromVecIn) const noexcept
            {
                return this->getX() == fromVecIn.getX() && this->getY() == fromVecIn.getY();
            }

            /**inline bool operator!() const noexcept
            {
                if(this->x != 0)
                    return false;
                else if(this->y != 0)
                    return false;
                return true;
            }

            inline bool operator||(const Vec2<T> &vecIn) const noexcept
            {
                if(this->x == 0 && this->y == 0)
                    return true;
                else if(vecIn.getX() == 0 && vecIn.getY() == 0)
                    return true;
                return false;
            }

            inline bool operator&&(const Vec2<T> &vecIn) const noexcept
            {
                if(this->x != 0)
                    return false;
                else if(this->y != 0)
                    return false;
                else if(vecIn.x != 0)
                    return false;
                else if(vecIn.y != 0)
                    return false;
                return true;
            }**/

            inline operator bool() const noexcept
            {
                if(this->getX() != 0)
                    return true;
                if(this->getY() != 0)
                    return true;
                return false;
            }

            inline std::string operator,(const Vec2<T> &vecIn) const noexcept
            {
                return this->toString() + ", " + vecIn.toString();
            }

            // Mathematics operator


            // +

            friend inline Vec2<T> operator+(const Vec2<T> &fromVecIn, const Vec2<T> &toVecIn) noexcept
            {
                return Vec2<T>(fromVecIn.getX() + toVecIn.getX(), fromVecIn.getY() + toVecIn.getY());
            }

            // +=

            inline Vec2<T> operator+=(const Vec2<T> &toVecIn) noexcept
            {
                this->x = this->x + toVecIn.getX();
                this->y = this->y + toVecIn.getY();
                return *this;
            }

            // ++

            inline Vec2<T> operator++() noexcept
            {
                this->x ++;
                this->y ++;
                return *this;
            }

            inline Vec2<T> operator++(int n) noexcept
            {
                if(n != 0)
                {
                    this->x = this->x + n;
                    this->y = this->y + n;
                }
                else
                {
                    this->x = this->x + 1;
                    this->y = this->y + 1;
                }
                return *this;
            }



            // -

            friend inline Vec2<T> operator-(const Vec2<T> &fromVecIn, const Vec2<T> &toVecIn) noexcept
            {
                return Vec2<T>(fromVecIn.getX() - toVecIn.getX(), fromVecIn.getY() - toVecIn.getY());
            }

            // -=

            inline Vec2<T> operator-=(const Vec2<T> &toVecIn) noexcept
            {
                this->x = this->x - toVecIn.getX();
                this->y = this->y - toVecIn.getY();
                return *this;
            }

            // --

            inline Vec2<T> operator--() noexcept
            {
                this->x = this->x - 1;
                this->y = this->y - 1;
                return *this;
            }

            inline Vec2<T> operator--(int n) noexcept
            {
                if(n != 0)
                {
                    this->x = this->x - n;
                    this->y = this->y - n;
                }
                else
                {
                    this->x = this->x - 1;
                    this->y = this->y - 1;
                }
                return *this;
            }


            // *

            friend inline Vec2<T> operator*(const Vec2<T> &fromVecIn, const Vec2<T> &toVecIn) noexcept
            {
                return Vec2<T>(fromVecIn.getX() * toVecIn.getX(), fromVecIn.getY() * toVecIn.getY());
            }

            // *=

            inline Vec2<T> operator*=(const Vec2<T> &toVecIn) noexcept
            {
                this->x = this->x * toVecIn.getX();
                this->y = this->y * toVecIn.getY();
                return *this;
            }


            // /

            friend inline Vec2<T> operator/(const Vec2<T> &fromVecIn, const Vec2<T> &toVecIn) noexcept
            {
                return Vec2<T>(fromVecIn.getX() / toVecIn.getX(), fromVecIn.getY() / toVecIn.getY());
            }

            // /=

            inline Vec2<T> operator/=(const Vec2<T> &toVecIn) noexcept
            {
                this->x = this->x / toVecIn.getX();
                this->y = this->y / toVecIn.getY();
                return *this;
            }

            // Conversion

            inline operator float()
            {
                return this->getX() * this->getX() + this->getY() * this->getY();
            }

            // Add

            inline auto& add(const T &xIn, const T &yIn) noexcept;
            inline auto& add(const Vec2<T> &vecIn) noexcept;
            inline auto& add(const Vec3<T> &vecIn) noexcept;
            inline auto& add(const Vec4<T> &vecIn) noexcept;
            inline auto& add(const Vec5<T> &vecIn) noexcept;
            inline auto& add(const Vec6<T> &vecIn) noexcept;

            inline auto& add(const T &valueIn) noexcept;
            inline auto& addX(const T &valueIn) noexcept;
            inline auto& addY(const T &valueIn) noexcept;


            // Subtract

            inline auto& sub(const T &valueIn) noexcept;
            inline auto& subX(const T &valueIn) noexcept;
            inline auto& subY(const T &valueIn) noexcept;

            inline auto& sub(const T &xIn, const T &yIn) noexcept;
            inline auto& sub(const Vec2<T> &vecIn) noexcept;
            inline auto& sub(const Vec3<T> &vecIn) noexcept;
            inline auto& sub(const Vec4<T> &vecIn) noexcept;
            inline auto& sub(const Vec5<T> &vecIn) noexcept;
            inline auto& sub(const Vec6<T> &vecIn) noexcept;


            // Multiply

            inline auto& mul(const T &valueIn) noexcept;
            inline auto& mulX(const T &valueIn) noexcept;
            inline auto& mulY(const T &valueIn) noexcept;

            inline auto& mul(const T &xIn, const T &yIn) noexcept;
            inline auto& mul(const Vec2<T> &vecIn) noexcept;
            inline auto& mul(const Vec3<T> &vecIn) noexcept;
            inline auto& mul(const Vec4<T> &vecIn) noexcept;
            inline auto& mul(const Vec5<T> &vecIn) noexcept;
            inline auto& mul(const Vec6<T> &vecIn) noexcept;


            // Divide

            inline auto& div(const T &valueIn) noexcept;
            inline auto& divX(const T &valueIn) noexcept;
            inline auto& divY(const T &valueIn) noexcept;

            inline auto& div(const T &xIn, const T &yIn) noexcept;
            inline auto& div(const Vec2<T> &vecIn) noexcept;
            inline auto& div(const Vec3<T> &vecIn) noexcept;
            inline auto& div(const Vec4<T> &vecIn) noexcept;
            inline auto& div(const Vec5<T> &vecIn) noexcept;
            inline auto& div(const Vec6<T> &vecIn) noexcept;


            // Dot (ScalarProduct)

            inline auto dot(const T &valueIn)  const   noexcept;
            inline auto dot(const T &xIn, const T &yIn)  const   noexcept;

            inline auto dot(const Vec2<T> &vecIn)  const   noexcept;
            inline auto dot(const Vec3<T> &vecIn)  const   noexcept;
            inline auto dot(const Vec4<T> &vecIn)  const   noexcept;
            inline auto dot(const Vec5<T> &vecIn)  const   noexcept;
            inline auto dot(const Vec6<T> &vecIn)  const   noexcept;


            // Setter

            inline auto& set(const Vec2<T> &vecIn) noexcept;
            inline auto& set(const Vec3<T> &vecIn) noexcept;
            inline auto& set(const Vec4<T> &vecIn) noexcept;
            inline auto& set(const Vec5<T> &vecIn) noexcept;
            inline auto& set(const Vec6<T> &vecIn) noexcept;
            inline auto& set(const T &xIn, const T &yIn) noexcept;
            inline auto& setX(const T &xIn) noexcept;
            inline auto& setY(const T &yIn) noexcept;

            // Getter

            [[nodiscard]] inline auto& getX() const noexcept { return this->x; }
            [[nodiscard]] inline auto& getY() const noexcept { return this->y; }

            [[nodiscard]]   inline std::string toString         ()  const   noexcept;
            [[nodiscard]]   inline std::array<T, 6> toArray ()  const   noexcept    { return { x, y }; }

        private:

            T x;
            T y;
    };
}

#include <synk/utils/maths/vectors/Vec2.inl>
