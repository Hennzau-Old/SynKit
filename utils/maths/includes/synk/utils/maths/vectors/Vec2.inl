
/*
 * Vec6.inl
 *
 *  Created on: 5 juin 2020
 *      Author: seynax
 */
#pragma once

#include <synk/utils/maths/vectors/Vec2.hpp>
#include <synk/utils/maths/vectors/Vec3.hpp>
#include <synk/utils/maths/vectors/Vec4.hpp>
#include <synk/utils/maths/vectors/Vec5.hpp>
#include <synk/utils/maths/vectors/Vec6.hpp>


namespace synk::utils::maths
{
    template<typename T>
    Vec2<T>::Vec2(const T &xIn, const T &yIn)
        : x { static_cast<T> ( xIn ) },
          y { static_cast<T> ( yIn ) }
    {
    }

    template<typename T>
    Vec2<T>::Vec2(const Vec2<T> &vecIn)
    :   x { static_cast<T> ( vecIn.getX() ) },
        y { static_cast<T> ( vecIn.getY() ) }
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
    }

    template<typename T>
    Vec2<T>::Vec2(const Vec3<T> &vecIn)
    :   x { static_cast<T> ( vecIn.getX() ) },
        y { static_cast<T> ( vecIn.getY() ) }
    {
    }

    template<typename T>
    Vec2<T>::Vec2(const Vec4<T> &vecIn)
    :   x { static_cast<T> ( vecIn.getX() ) },
        y { static_cast<T> ( vecIn.getY() ) }
    {
    }

    template<typename T>
    Vec2<T>::Vec2(const Vec5<T> &vecIn)
    :   x { static_cast<T> ( vecIn.getX() ) },
        y { static_cast<T> ( vecIn.getY() ) }
    {
    }

    template<typename T>
    Vec2<T>::Vec2(const Vec6<T> &vecIn)
    :   x { static_cast<T> ( vecIn.getX() ) },
        y { static_cast<T> ( vecIn.getY() ) }
    {
    }


    template<typename T>
    Vec2<T>::~Vec2()
    {

    }


    // Add

    template<typename T>
    inline auto& Vec2<T>::add(const T &valueIn) noexcept
    {
        this->x += valueIn;
        this->y += valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::addX(const T &valueIn) noexcept
    {
        this->x += valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::addY(const T &valueIn) noexcept
    {
        this->y += valueIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::add(const T &xIn, const T &yIn) noexcept
    {
        this->x += xIn;
        this->y += yIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::add(const Vec2<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::add(const Vec3<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::add(const Vec4<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::add(const Vec5<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::add(const Vec6<T> &vecIn) noexcept
    {
        this->x += vecIn.getX();
        this->y += vecIn.getY();
        return *this;
    }


    // Subtract

    template<typename T>
    inline auto& Vec2<T>::sub(const T &valueIn) noexcept
    {
        this->x -= valueIn;
        this->y -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::subX(const T &valueIn) noexcept
    {
        this->x -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::subY(const T &valueIn) noexcept
    {
        this->y -= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::sub(const Vec2<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::sub(const Vec3<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::sub(const Vec4<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::sub(const Vec5<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::sub(const Vec6<T> &vecIn) noexcept
    {
        this->x -= vecIn.getX();
        this->y -= vecIn.getY();
        return *this;
    }


    // Multiply

    template<typename T>
    inline auto& Vec2<T>::mul(const T &valueIn) noexcept
    {
        this->x *= valueIn;
        this->y *= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::mulX(const T &valueIn) noexcept
    {
        this->x *= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::mulY(const T &valueIn) noexcept
    {
        this->y *= valueIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::mul(const T &xIn, const T &yIn) noexcept
    {
        this->x *= xIn;
        this->y *= yIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::mul(const Vec2<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::mul(const Vec3<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::mul(const Vec4<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::mul(const Vec5<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::mul(const Vec6<T> &vecIn) noexcept
    {
        this->x *= vecIn.getX();
        this->y *= vecIn.getY();
        return *this;
    }


    // Divide

    template<typename T>
    inline auto& Vec2<T>::div(const T &valueIn) noexcept
    {
        this->x /= valueIn;
        this->y /= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::divX(const T &valueIn) noexcept
    {
        this->x /= valueIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::divY(const T &valueIn) noexcept
    {
        this->y /= valueIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::div(const T &xIn, const T &yIn) noexcept
    {
        this->x /= xIn;
        this->y /= yIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::div(const Vec2<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::div(const Vec3<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::div(const Vec4<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::div(const Vec5<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::div(const Vec6<T> &vecIn) noexcept
    {
        this->x /= vecIn.getX();
        this->y /= vecIn.getY();
        return *this;
    }


    // Dot (ScalarProduct)

    template<typename T>
    inline auto Vec2<T>::dot(const T &valueIn) const noexcept
    {
        return this->x * valueIn + this->y * valueIn;
    }

    template<typename T>
    inline auto Vec2<T>::dot(const T &xIn, const T &yIn) const noexcept
    {
        return this->x * xIn + this->y * yIn;
    }

    template<typename T>
    inline auto Vec2<T>::dot(const Vec2<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY();
    }

    template<typename T>
    inline auto Vec2<T>::dot(const Vec3<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY();
    }

    template<typename T>
    inline auto Vec2<T>::dot(const Vec4<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY();
    }

    template<typename T>
    inline auto Vec2<T>::dot(const Vec5<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY();
    }

    template<typename T>
    inline auto Vec2<T>::dot(const Vec6<T> &vecIn) const noexcept
    {
        return this->x * vecIn.getX() + this->y * vecIn.getY();
    }


    // Setter

    template<typename T>
    inline auto& Vec2<T>::setX(const T &xIn) noexcept
    {
        this->x = xIn;
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::setY(const T &yIn) noexcept
    {
        this->y = yIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::set(const T &xIn, const T &yIn) noexcept
    {
        this->x = xIn;
        this->y = yIn;
        return *this;
    }


    template<typename T>
    inline auto& Vec2<T>::set(const Vec2<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::set(const Vec3<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::set(const Vec4<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::set(const Vec5<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        return *this;
    }

    template<typename T>
    inline auto& Vec2<T>::set(const Vec6<T> &vecIn) noexcept
    {
        this->x = vecIn.getX();
        this->y = vecIn.getY();
        return *this;
    }


    // toString

    template<typename T>
    [[nodiscard]] inline std::string Vec2<T>::toString() const noexcept
    {
        return "[X: " + std::to_string(this->x) + ", Y: " + std::to_string(this->y) + "]";
    }
}
