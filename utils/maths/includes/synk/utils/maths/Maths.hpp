#pragma once

#include <ostream>
#include <string>
#include <array>
#include <vector>

#include <synk/utils/maths/vectors/Vec2.hpp>
#include <synk/utils/maths/vectors/Vec3.hpp>
#include <synk/utils/maths/vectors/Vec4.hpp>
#include <synk/utils/maths/vectors/Vec5.hpp>
#include <synk/utils/maths/vectors/Vec6.hpp>

namespace synk::utils::maths
{

    // std::uint8_t, std::uint16_t, std::uint32_t
    // std::int8_t, std::int16_t, std::int32_t
    // int, bool, byte, short float, long

    using Vec2bool = Vec2<bool>;
    using Vec3bool = Vec3<bool>;
    using Vec4bool = Vec4<bool>;
    using Vec5bool = Vec5<bool>;
    using Vec6bool = Vec6<bool>;

    using Vec2ui = Vec2<std::uint8_t>;
    using Vec3ui = Vec3<std::uint8_t>;
    using Vec4ui = Vec4<std::uint8_t>;
    using Vec5ui = Vec5<std::uint8_t>;
    using Vec6ui = Vec6<std::uint8_t>;

    using Vec2i = Vec2<std::int8_t>;
    using Vec3i = Vec3<std::int8_t>;
    using Vec4i = Vec4<std::int8_t>;
    using Vec5i = Vec5<std::int8_t>;
    using Vec6i = Vec6<std::int8_t>;

    using Vec2uii6 = Vec2<std::uint16_t>;
    using Vec3ui16 = Vec3<std::uint16_t>;
    using Vec4ui16 = Vec4<std::uint16_t>;
    using Vec5ui16 = Vec5<std::uint16_t>;
    using Vec6ui16 = Vec6<std::uint16_t>;

    using Vec2i16 = Vec2<std::int16_t>;
    using Vec3fi16 = Vec3<std::int16_t>;
    using Vec4fi16 = Vec4<std::int16_t>;
    using Vec5fi16 = Vec5<std::int16_t>;
    using Vec6fi16 = Vec6<std::int16_t>;

    using Vec2ui32 = Vec2<std::uint32_t>;
    using Vec3ui32 = Vec3<std::uint32_t>;
    using Vec4ui32 = Vec4<std::uint32_t>;
    using Vec5ui32 = Vec5<std::uint32_t>;
    using Vec6ui32 = Vec6<std::uint32_t>;

    using Vec2i32 = Vec2<std::int32_t>;
    using Vec3i32 = Vec3<std::int32_t>;
    using Vec4i32 = Vec4<std::int32_t>;
    using Vec5i32 = Vec5<std::int32_t>;
    using Vec6i32 = Vec6<std::int32_t>;

    using Vec2s = Vec2<short>;
    using Vec3s = Vec3<short>;
    using Vec4s = Vec4<short>;
    using Vec5s = Vec5<short>;
    using Vec6s = Vec6<short>;

    using Vec2f = Vec2<float>;
    using Vec3f = Vec3<float>;
    using Vec4f = Vec4<float>;
    using Vec5f = Vec5<float>;
    using Vec6f = Vec6<float>;

    using Vec2l = Vec2<long>;
    using Vec3l = Vec3<long>;
    using Vec4l = Vec4<long>;
    using Vec5l = Vec5<long>;
    using Vec6l = Vec6<long>;

    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec2<T>& vecIn);
    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec3<T>& vecIn);
    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec4<T>& vecIn);
    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec5<T>& vecIn);
    template<typename T>
    std::ostream &operator << (std::ostream& out, const Vec6<T>& vecIn);

    template<typename T>
    auto addVec6toVector(const std::vector<T> &vectorIn, const Vec6<T> &vecIn) noexcept;

    template<typename T, std::size_t Y>
    auto addVec6ToArray(const std::array<T, Y> &arrIn, const Vec6<T> &vecIn) noexcept;

    template<typename T>
    auto dist(const T &fromTIn, const T &toTIn) noexcept;

    template<typename T>
    auto dist(const T &fromTIn) noexcept;


    // Vec2 distance

    template<typename T>
    auto dist(const Vec2<T> &fromTIn, const Vec2<T> &toTIn) noexcept;

    template<typename T>
    auto dist(const Vec2<T> &fromTIn) noexcept;


    // Vec3 distance

    template<typename T>
    auto dist(const Vec3<T> &fromTIn, const Vec3<T> &toTIn) noexcept;

    template<typename T>
    auto dist(const Vec3<T> &fromTIn) noexcept;


    // Vec4 distance

    template<typename T>
    auto dist(const Vec4<T> &fromTIn, const Vec4<T> &toTIn) noexcept;

    template<typename T>
    auto dist(const Vec4<T> &fromTIn) noexcept;


    // Vec5 distance

    template<typename T>
    auto dist(const Vec5<T> &fromTIn, const Vec5<T> &toTIn) noexcept;

    template<typename T>
    auto dist(const Vec5<T> &fromTIn) noexcept;


    // Vec6 distance

    template<typename T>
    auto dist(const Vec6<T> &fromTIn, const Vec6<T> &toTIn) noexcept;

    template<typename T>
    auto dist(const Vec6<T> &fromTIn) noexcept;
}

#include <synk/utils/maths/Maths.inl>
