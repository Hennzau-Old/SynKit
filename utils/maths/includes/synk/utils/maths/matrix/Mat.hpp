#pragma once

#include <iostream>

namespace synk::utils::maths
{
    template<typename T, std::size_t r, std::size_t c>
    class Mat
    {
    public:
        Mat(std::array<T, r * c> arrIn);
    private:
        std::array<T, r * c> arr;
    };
}
