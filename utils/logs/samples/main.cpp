#include <iostream>

#include <synk/utils/logs/Logger.hpp>

int main(void)
{
    using namespace synk::utils::logs;

    const auto  main_module =   Logger::Module  { "MAIN" };
    const auto  test_module =   Logger::Module  { "GAME" };
    const auto  sub_module  =   Logger::Module  { "main.cpp" };

    Logger::setFocus(Logger::Level::TEXT);

    Logger::init(main_module);

        Logger::tLog(sub_module, "Hello World!");
        Logger::iLog(sub_module, "Hiiiiiii !");

        Logger::init(test_module);

            Logger::sLog(sub_module, "How are you ?");
            Logger::wLog(sub_module, "I'm fine and you ?");

        Logger::exit(test_module);

        Logger::eLog(sub_module, "Not really good this morning !");
        Logger::fLog(sub_module, "Okay Goodbye !");

    Logger::exit(main_module);
}
