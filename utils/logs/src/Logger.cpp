#include <synk/utils/logs/Logger.hpp>

#ifdef _WIN32
#include <windows.h>
#endif


using namespace synk::utils::logs;

auto    tab_number = 0u;
auto    severity_focus  =   Logger::Level::TEXT;

void Logger::init(const Module &module) noexcept
{
    setColor(Level::ENTER_OR_EXIT);
    printTab();

        std::cout << "------------[_" << module.name << "_]------------" << std::endl;
    resetColor();

    tab_number++;
}

void Logger::exit(const Module &module) noexcept
{
    tab_number--;

    setColor(Level::ENTER_OR_EXIT);
    printTab();

        std::cout << "------------[/" << module.name << "/]------------" << std::endl;
    resetColor();
}

void Logger::setFocus(const Level &level)   noexcept
{
    severity_focus  =   level;
}

void Logger::setColor(const Level &level)   noexcept
{
    #ifdef __linux__

        switch(level)
        {
            case Level::TEXT:
                std::cout << WHITE;
                break;
            case Level::INFO:
                std::cout << CYAN;
                break;
            case Level::SUCCESS:
                std::cout << GREEN;
                break;
            case Level::WARNING:
                std::cout << YELLOW;
                break;
            case Level::NON_FATAL_ERROR:
                std::cout << BOLDYELLOW;
                break;
            case Level::FATAL_ERROR:
                std::cout << BOLDRED;
                break;
            case Level::ENTER_OR_EXIT:
                std::cout << MAGENTA;
        }

    #endif

    #ifdef _WIN32

        HANDLE cout_handle  =   GetStdHandle(STD_OUTPUT_HANDLE);

        switch(level)
        {
            case Level::TEXT:
                SetConsoleTextAttribute(cout_handle, 15);
                break;
            case Level::INFO:
                SetConsoleTextAttribute(cout_handle, 63);
                break;
            case Level::SUCCESS:
                SetConsoleTextAttribute(cout_handle, 47);
                break;
            case Level::WARNING:
                SetConsoleTextAttribute(cout_handle, 111);
                break;
            case Level::NON_FATAL_ERROR:
                SetConsoleTextAttribute(cout_handle, 207);
                break;
            case Level::FATAL_ERROR:
                SetConsoleTextAttribute(cout_handle, 79);
                break;
            case Level::ENTER_OR_EXIT:
                SetConsoleTextAttribute(cout_handle, 105);
        }

    #endif
}

void Logger::resetColor()   noexcept
{
    #ifdef __linux__

        std::cout << RESET;

    #endif

    #ifdef _WIN32

        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);

    #endif
}

void Logger::tLog(const Module &module, const std::string &text)    noexcept
{
    print(Level::TEXT, module, text);
}

void Logger::iLog(const Module &module, const std::string &text)    noexcept
{
    print(Level::INFO, module, text);
}

void Logger::sLog(const Module &module, const std::string &text)    noexcept
{
    print(Level::SUCCESS, module, text);
}

void Logger::wLog(const Module &module, const std::string &text)    noexcept
{
    print(Level::WARNING, module, text);
}

void Logger::eLog(const Module &module, const std::string &text)    noexcept
{
    print(Level::NON_FATAL_ERROR, module, text);
}

void Logger::fLog(const Module &module, const std::string &text)    noexcept
{
    print(Level::FATAL_ERROR, module, text);
}

void Logger::print(const Level &level, const Module &module, const std::string &text) noexcept
{
    if (level < severity_focus)
    {
        return;
    }

    setColor(level);
    printTab();

    printSeverity(level);

    std::cout << " : " << module.name << " -> " << text << std::endl;

    resetColor();
}

void Logger::printSeverity(const Level &level)  noexcept
{
    auto text   =   std::string {};

    switch (level)
    {
        case Level::TEXT:
            text    =   "[_____TEXT______]";
            break;
        case Level::INFO:
            text    =   "[_____INFO______]";
            break;
        case Level::SUCCESS:
            text    =   "[____SUCCESS____]";
            break;
        case Level::WARNING:
            text    =   "[____WARNING____]";
            break;
        case Level::NON_FATAL_ERROR:
            text    =   "[_____ERROR_____]";
            break;
        case Level::FATAL_ERROR:
            text    =   "[__FATAL_ERROR__]";
            break;
    }

    std::cout << text;
}

void Logger::printTab() noexcept
{
    for (unsigned int i = 0; i < tab_number; i++)
    {
        std::cout << "      ";
    }
}
