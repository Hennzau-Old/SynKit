#pragma once

#include <iostream>
#include <string>

#ifdef __linux__
#define RESET   "\033[0m"
#define BLACK   "\033[30m"
#define RED     "\033[31m"
#define GREEN   "\033[32m"
#define YELLOW  "\033[33m"
#define BLUE    "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN    "\033[36m"
#define WHITE   "\033[37m"

#define BOLDBLACK   "\033[1m\033[30m"
#define BOLDRED     "\033[1m\033[31m"
#define BOLDGREEN   "\033[1m\033[32m"
#define BOLDYELLOW  "\033[1m\033[33m"
#define BOLDBLUE    "\033[1m\033[34m"
#define BOLDMAGENTA "\033[1m\033[35m"
#define BOLDCYAN    "\033[1m\033[36m"
#define BOLDWHITE   "\033[1m\033[37m"
#endif

namespace synk::utils::logs
{
    class Logger
    {
        public:

            enum class Level
            {
                TEXT,
                INFO,
                SUCCESS,
                WARNING,
                NON_FATAL_ERROR,
                FATAL_ERROR,
                ENTER_OR_EXIT
            };

            struct Module
            {
                std::string name;
            };

            static void         init(const Module& module)  noexcept;
            static void         exit(const Module& module)  noexcept;

            static void         setFocus(const Level& level)    noexcept;

            static void         print   (const Level& level, const Module& module, const std::string& text)   noexcept;

            static void         tLog(const Module& module, const std::string& text) noexcept;
            static void         iLog(const Module& module, const std::string& text) noexcept;
            static void         sLog(const Module& module, const std::string& text) noexcept;
            static void         wLog(const Module& module, const std::string& text) noexcept;
            static void         eLog(const Module& module, const std::string& text) noexcept;
            static void         fLog(const Module& module, const std::string& text) noexcept;

        private:

            static void         setColor        (const Level& level)    noexcept;
            static void         printSeverity   (const Level& level)    noexcept;
            static void         resetColor      ()  noexcept;
            static void         printTab        ()  noexcept;
    };   
}
