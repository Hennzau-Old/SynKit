cmake_minimum_required(VERSION 3.16)

project(
    SynKit_Utils_Utilities
    CXX
)

file(
    GLOB_RECURSE

    utilities_headers

    includes/*.hpp
)

file(
    GLOB_RECURSE

    utilities_inlines

    includes/*.inl
)

file(
    GLOB_RECURSE

    utilities_sources

    src/*.cpp
)

add_library(
    SynKit_Utils_Utilities

    ${utilities_headers}
    ${utilities_inlines}
    ${utilities_sources}
)

target_include_directories(
    SynKit_Utils_Utilities

    PUBLIC
    includes/
    libs/
)

target_compile_features(
    SynKit_Utils_Utilities

    PUBLIC
    cxx_std_20
)

set_target_properties(
    SynKit_Utils_Utilities

    PROPERTIES
    LINKER_LANGUAGE
    CXX
)

if (SYNKIT_UTILS_UTILITIES_EXAMPLES)

    file(
        GLOB_RECURSE

        utilities_samples

        samples/*cpp
    )

    add_executable(
        SynKit_Utils_Utilities_Examples

        ${utilities_samples}
    )

    target_link_libraries(
        SynKit_Utils_Utilities_Examples

        PRIVATE
        SynKit_Utils_Utilities
    )

endif()

