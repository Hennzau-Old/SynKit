#include <iostream>

#include <synk/utils/utilities/File.hpp>
#include <synk/utils/utilities/Memory.hpp>
#include <synk/utils/utilities/NonCopyable.hpp>

class Example : synk::utils::utilities::NonCopyable // We can't make a copy of our object
{
    public:

        Example ()
        {

        }
        ~Example()
        {

        }

        void print  ()
        {
            std::cout << "Hello World !" << std::endl;
        }
};

DECLARE_PTR_AND_REF(Example)    //  ->  automatic create all "Example[TYPES]Ptr"

int main(void)
{
    using namespace synk::utils::utilities;

    ExampleOwnedPtr my_test_ptr             =   std::make_unique<Example> ();   // Create a unique_ptr<Example>
    ExampleObserverPtr  my_test_observer    =   makeObserver(my_test_ptr);      // create an observer_ptr

    // File reading //

    std::string text    =   File::readFile("res/document.txt");
    std::cout << text << std::endl;
}
