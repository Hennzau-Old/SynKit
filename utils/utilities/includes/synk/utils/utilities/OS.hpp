#pragma once

#include <iostream>

/**
 * _WIN32_                                  // Windows 32
 * _WIN64_                                  // Windows 64
 * __APPLE__                                // Apple
 * __MACM__                                 // Alternative Apple
 * TARGET_OS_EMBEDDED                       // IOS
 * TARGET_IPHONE_SIMULATOR                  // Simulateur IOS
 * TARGET_OS_MAC                            // MacOS
 * __ANDROID__                              // Android
 * __unix__                                 // Unix
 * __linux__                                // Linux
 * _POSIX_VERSION                           // Basé sur Posix
 * __sun                                    // Solaris
 * __hpux                                   // HP UX
 * BSD                                      // BSD
 * __DragonFly__                            // DragonFly BSD
 * __FreeBSD__                              // FreeBSD
 * __NetBSD__                               // NetBSD
 * __OpenBSD__                              // OpenBSD
 **/

namespace synk::utils::utilities::OS
{
    enum class OS
    {
        WINDOWS,
        APPLE,
        MACM,
        IOS,
        IOS_SIM,
        MACOS,
        ANDROID,
        UNIX,
        LINUX,
        POSIX,
        SUN,
        HPUX,
        BSD,
        DRAGON_FLY,
        FREEBSD,
        NETBSD,
        OPENBSD
    };

    #ifdef _WIN64

            static const OS SYNK_OS = OS::WINDOWS;
            #define SYNK_OS_WINDOWS true

    #elif _WIN32

            static const OS SYNK_OS = OS::WINDOWS;
            #define SYNK_OS_WINDOWS true

    #elif TARGET_OS_MAC

            static const OS SYNK_OS = OS::MACOS;
            #define SYNK_OS_MACOS true

    #elif __APPLE__

            static const OS SYNK_OS = OS::APPLE;

    #elif __MACM__

            static const OS SYNK_OS = OS::MACM;

    #elif TARGET_OS_EMBEDDED
            OS SYNK_OS = OS::IOS;

    #elif TARGET_IPHONE_SIMULTOR

            static const OS SYNK_OS = OS::IOS_SIM;

    #elif __ANDROID__

            static const OS SYNK_OS = OS::ANDROID;

    #elif __linux__

            static const OS SYNK_OS = OS::LINUX;
            #define SYNK_OS_LINUX true

    #elif __unix__

            static const OS SYNK_OS = OS::UNIX;

    #elif _POSIX_VERSION

            static const OS SYNK_OS = OS::POSIX;

    #elif __sun

            static const OS SYNK_OS = OS::SUN;

    #elif __hpux

            static const OS SYNK_OS = OS::HPUX;

    #elif BSD

            static const OS SYNK_OS = OS::BSD;

    #elif __DragonFly__

            static const OS SYNK_OS = OS::DRAGONFLY;

    #elif __FreeBSD__

            static const OS SYNK_OS = OS::FREEBSD;

    #elif __NetBSD__

            static const OS SYNK_OS = OS::NETBSD;

    #elif __OpenBSD__

            static const OS SYNK_OS = OS::OPENBSD;
    #endif

    static inline auto  show()
    {
        switch(SYNK_OS)
        {
            case OS::WINDOWS:
                std::cout << "You are on Windows operating system" << std::endl;
            break;
            case OS::MACOS:
                std::cout << "You are on MacOS operating system" << std::endl;
            break;
            case OS::LINUX:
                std::cout << "You are on Linux operating system" << std::endl;
            break;
            default:
                std::cout << "Unkown operating system" << std::endl;
        }
    }
}
