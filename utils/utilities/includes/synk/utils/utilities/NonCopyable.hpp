// Copryright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distributio

#pragma once

namespace synk::utils::utilities
{
    class NonCopyable
    {
      public:

        constexpr NonCopyable() noexcept = default;

        ~NonCopyable() noexcept                     = default;
        NonCopyable (NonCopyable &&) noexcept       = default;
        NonCopyable (const NonCopyable &) noexcept  = delete;

        NonCopyable &operator=  (NonCopyable &&) noexcept       = default;
        void operator=          (const NonCopyable &) noexcept  = delete;
    };
}
