#pragma once

#include <iostream>
#include <vector>
#include <array>
#include <stack>
#include <algorithm>

namespace synk::utils::utilities
{
    template<std::size_t x, std::size_t y>
    class QuadSplitter
    {
        public:

            struct Quad
            {
                std::uint32_t   type;

                std::uint32_t   pos_x;
                std::uint32_t   pos_y;
                std::uint32_t   size_x;
                std::uint32_t   size_y;
            };

            QuadSplitter    () {}
            ~QuadSplitter   () {}

            std::vector<Quad>   split   (const std::array<std::array<std::uint32_t, y>, x>& board)  noexcept;

            bool            caseOk  (const std::array<std::array<std::uint32_t, y>, x>& board, const std::uint32_t& pos_x, const std::uint32_t& pos_y, const std::uint32_t& type)   const noexcept;
            bool            lineOk  (const std::array<std::array<std::uint32_t, y>, x>& board, const std::uint32_t& i, const std::uint32_t& limit_x, const std::uint32_t& limit_y, const std::uint32_t& type)   const   noexcept;
            void            remove  (std::array<std::array<std::uint32_t, y>, x>& board, const Quad& quad)  noexcept;
            std::uint32_t   getCase (const std::array<std::array<std::uint32_t, y>, x>& board, const std::uint32_t& pos_x, const std::uint32_t& pos_y)   const noexcept;

        public:

            template <std::size_t x, std::size_t y>
            std::vector<QuadSplitter::Quad>   QuadSplitter::split(const std::array<std::array<std::uint32_t, y>, x> &board)   noexcept
            {
                auto    matrix  =   board;
                auto    results =   std::vector<Quad>   { };

                auto    x   =   0u;
                auto    y   =   0u;
                auto    w   =   0u;
                auto    h   =   0u;
                auto    c   =   0u;

                for (auto i { 0 }; i < board.size(); i++)
                {
                    for (auto j { 0 }; j < board[0].size(); j++)
                    {
                        if (getCase(matrix, j, i) == 0)
                        {
                            continue;
                        }

                        c   =   getCase(matrix, j, i);
                        x   =   i;
                        y   =   j;
                        w   =   1;
                        h   =   1;

                        {
                            auto    cursor_x    =   j;
                            auto    cursor_y    =   i;

                            cursor_x++;

                            while(caseOk(matrix, cursor_x, cursor_y, c))
                            {
                                w++;
                                cursor_x++;
                            }
                        }

                        while(lineOk(matrix, i + (h - 1), y, y + w, c))
                        {
                            h++;
                        }

                        results.push_back( {c, x, y, h, w} );
                        remove(matrix, { 0, x, y, w, h} );
                    }
                }

                return results;
            }

            template <std::size_t x, std::size_t y>
            bool QuadSplitter<x, y>::caseOk(const std::array<std::array<std::uint32_t, y>, x> &board, const std::uint32_t &pos_x, const std::uint32_t &pos_y, const std::uint32_t &type)  const   noexcept
            {
                if (board[pos_y][pos_x] == type)
                {
                    return true;
                }

                return false;
            }

            template <std::size_t x, std::size_t y>
            bool QuadSplitter::lineOk(const std::array<std::array<std::uint32_t, y>, x> &board, const std::uint32_t &i, const std::uint32_t &limit_x, const std::uint32_t &limit_y, const std::uint32_t &type)  const   noexcept
            {
                const auto  start   =   limit_x;
                const auto  end     =   limit_y;

                if (i + 1 >= board.size())
                {
                    return false;
                }

                for (auto a = start; a < end; a++)
                {
                    if (board[i + 1][a] != type)
                    {
                        return false;
                    }
                }

                return true;
            }

            template <std::size_t x, std::size_t y>
            void QuadSplitter::remove(std::array<std::array<std::uint32_t, y>, x> &board, const QuadSplitter::Quad &quad) noexcept
            {
                for (auto i = quad.pos_x; i < quad.pos_x + quad.size_x; i++)
                {
                    for (auto j = quad.pos_y; j < quad.pos_y + quad.size_y; j++)
                    {
                        board[j][i] =   0;
                    }
                }
            }

            template <std::size_t x, std::size_t y>
            std::uint32_t   QuadSplitter::getCase(const std::array<std::array<std::uint32_t, y>, x> &board, const std::uint32_t &pos_x, const std::uint32_t &pos_y) const   noexcept
            {
                return board[pos_y][pos_x];
            }
    };
}

#include <synk/utils/utilities/QuadSplitter.inl>
