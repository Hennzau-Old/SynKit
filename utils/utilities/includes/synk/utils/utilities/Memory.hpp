// Copryright (C) 2019 Arthur LAURENT <arthur.laurent4@gmail.com>
// This file is subject to the license terms in the LICENSE file
// found in the top-level of this distributio

#pragma once

#include <array>
#include <memory>
#include <optional>
#include <type_traits>
#include <vector>

namespace _std
{
    template<typename T>

    class observer_ptr
    {
      public:
        using element_type = T;

        constexpr observer_ptr() noexcept = default;
        constexpr observer_ptr(std::nullptr_t) noexcept {}

        template<typename U,
                 typename = std::enable_if<!std::is_same_v<element_type, U> &&
                                           std::is_convertible_v<U *, element_type *>>>
        observer_ptr(observer_ptr<U> const &other)
            : observer_ptr(static_cast<element_type *>(other.get())) {}

        explicit observer_ptr(element_type *ptr) : _data(ptr) {}

        constexpr element_type *release() noexcept {
            auto *ptr = _data;
            _data     = nullptr;
            return ptr;
        }

        constexpr void reset(element_type *p = nullptr) noexcept { _data = p; }

        constexpr void swap(observer_ptr &other) noexcept {
            using std::swap;
            swap(_data, other._data);
        }

        constexpr friend void swap(observer_ptr &lhs, observer_ptr &rhs) noexcept { lhs.swap(rhs); }

        [[nodiscard]] constexpr element_type *get() const noexcept { return _data; }

        [[nodiscard]] constexpr std::add_lvalue_reference_t<element_type> operator*() const {
            return *get();
        }

        [[nodiscard]] constexpr element_type *operator->() const noexcept { return get(); }

        [[nodiscard]] constexpr explicit operator bool() const noexcept { return _data != nullptr; }

        [[nodiscard]] constexpr explicit operator element_type *() const noexcept { return get(); }

      private:
        element_type *_data = nullptr;
    };

    template<typename T>
    [[nodiscard]] observer_ptr<T> make_observer(T *ptr) noexcept {
        return observer_ptr<T>(ptr);
    }

    template<typename T1, typename T2>
    [[nodiscard]] bool operator==(observer_ptr<T1> const &p1, observer_ptr<T2> const &p2) {
        return p1.get() == p2.get();
    }

    template<typename T1, typename T2>
    [[nodiscard]] bool operator!=(observer_ptr<T1> const &p1, observer_ptr<T2> const &p2) {
        return !(p1 == p2);
    }

    template<typename T>
    [[nodiscard]] bool operator==(observer_ptr<T> const &p, std::nullptr_t) noexcept {
        return p.get() == nullptr;
    }

    template<typename T>
    [[nodiscard]] bool operator==(std::nullptr_t, observer_ptr<T> const &p) noexcept {
        return p.get() == nullptr;
    }

    template<typename T>
    [[nodiscard]] bool operator!=(observer_ptr<T> const &p, std::nullptr_t) noexcept {
        return p.get() != nullptr;
    }

    template<typename T>
    [[nodiscard]] bool operator!=(std::nullptr_t, observer_ptr<T> const &p) noexcept {
        return p.get() != nullptr;
    }

    template<typename T1, typename T2>
    [[nodiscard]] bool operator<(observer_ptr<T1> const &p1, observer_ptr<T2> const &p2) {
        return p1.get() < p2.get();
    }

    template<typename T1, typename T2>
    [[nodiscard]] bool operator>(observer_ptr<T1> const &p1, observer_ptr<T2> const &p2) {
        return p2 < p1;
    }

    template<typename T1, typename T2>
    [[nodiscard]] bool operator<=(observer_ptr<T1> const &p1, observer_ptr<T2> const &p2) {
        return !(p2 < p1);
    }

    template<typename T1, typename T2>
    [[nodiscard]] bool operator>=(observer_ptr<T1> const &p1, observer_ptr<T2> const &p2) {
        return !(p1 < p2);
    }
}

////////////

namespace synk::utils::utilities
{
    template<typename T>
    inline auto makeObserver(std::unique_ptr<T> &ptr)
    {
        return _std::make_observer<T>(ptr.get());
    }

    template<typename T>
    inline auto makeObserver(std::shared_ptr<T> &ptr)
    {
        return _std::make_observer<T>(ptr.get());
    }

    template<typename T>
    inline auto makeObserver(std::weak_ptr<T> &ptr)
    {
        return _std::make_observer<T>(ptr.get());
    }

    template<typename T>
    inline auto makeObserver(_std::observer_ptr<T> &ptr)
    {
        return _std::make_observer<T>(ptr.get());
    }

    template<typename T>
    inline auto makeObserver(T *ptr)
    {
        return _std::make_observer<T>(ptr);
    }

    template<typename T>
    inline auto makeObserver(T &ptr)
    {
        return _std::make_observer<T>(&ptr);
    }

    template<typename... Args>
    inline auto makeObserversArray(Args &&... args)
    {
        const auto init { (makeObserver(std::forward<Args>(args)), ...) };

        using ArrayType = decltype(init);
        using ValueType = typename ArrayType::value_type;

        return std::array<ValueType, sizeof...(args)> { (makeObserver(std::forward<Args>(args)),
                                                         ...) };
    }

    template<typename... Args>
    inline auto makeObservers(Args &&... args)
    {
        const auto init = { (makeObserver(std::forward<Args>(args)), ...) };

        using ArrayType = decltype(init);
        using ValueType = typename ArrayType::value_type;

        auto vec = std::vector<ValueType> { std::move(init) };
        return vec;
    }

    template<typename Container>
    inline auto toObservers(Container &&container)
    {
        using value_type = typename std::remove_reference_t<Container>::value_type;

        auto vec = std::vector<_std::observer_ptr<value_type>> {};
        vec.reserve(std::size(container));

        std::transform(std::cbegin(container),
                       std::cend(container),
                       std::back_inserter(vec),
                       [](auto &item) { return makeConstObserver(item); });

        return vec;
    }

    template<typename T>
    inline auto makeConstObserver(std::unique_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(std::shared_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(std::weak_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(_std::observer_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(T *ptr)
    {
        return _std::make_observer<const T>(ptr);
    }

    template<typename T>
    inline auto makeConstObserver(const std::unique_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(const std::shared_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(const std::weak_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(const _std::observer_ptr<T> &ptr)
    {
        return _std::make_observer<const T>(ptr.get());
    }

    template<typename T>
    inline auto makeConstObserver(const T *ptr)
    {
        return _std::make_observer<const T>(ptr);
    }

    template<typename T>
    inline auto makeConstObserver(const T &ptr)
    {
        return _std::make_observer<const T>(&ptr);
    }

    template<typename... Args>
    inline auto makeConstObserversArray(Args &&... args)
    {
        const auto init = { (makeConstObserver(std::forward<Args>(args)), ...) };

        using ArrayType = decltype(init);
        using ValueType = typename ArrayType::value_type;

        auto array = std::array<ValueType, sizeof...(args)> {};

        auto i = 0u;
        ((array[i++] = makeConstObserver(std::forward<Args>(args))), ...);

        return array;
    }

    template<typename... Args>
    inline auto makeConstObservers(Args &&... args)
    {
        const auto init = { (makeConstObserver(std::forward<Args>(args)), ...) };

        using ArrayType = decltype(init);
        using ValueType = typename ArrayType::value_type;

        auto vec = std::vector<ValueType> {};
        vec.reserve(sizeof...(args));
        (..., vec.emplace_back(makeConstObserver(std::forward<Args>(args))));

        return vec;
    }

    template<typename Container>
    inline auto toConstObservers(Container &&container)
    {
        using value_type = typename std::remove_reference_t<Container>::value_type;

        auto vec = std::vector<_std::observer_ptr<const value_type>> {};
        vec.reserve(std::size(container));

        std::transform(std::cbegin(container),
                       std::cend(container),
                       std::back_inserter(vec),
                       [](const auto &item) { return makeConstObserver(item); });

        return vec;
    }

    ////////////////

    #define DECLARE_PTR_AND_REF_(x)                                  \
        using x##ObserverPtr      = _std::observer_ptr<x>;           \
        using x##ConstObserverPtr = _std::observer_ptr<const x>;     \
        using x##OwnedPtr         = std::unique_ptr<x>;              \
        using x##SharedPtr        = std::shared_ptr<x>;              \
        using x##ConstSharedPtr   = std::shared_ptr<const x>;        \
        using x##WeakPtr          = std::weak_ptr<x>;                \
        using x##ConstWeakPtr     = std::weak_ptr<const x>;          \
        using x##Ref              = std::reference_wrapper<x>;       \
        using x##CRef             = std::reference_wrapper<const x>; \
        using x##Opt              = std::optional<x>;

    #define DECLARE_PTR_AND_REF(type) DECLARE_PTR_AND_REF_(type)
}
