namespace synk::utils::utilities::Data
{
    template<typename T>
    auto toArray(const std::vector<T> &vectorIn) noexcept
    {
        std::array<T, vectorIn.size()> arr;
        std::uint32_t i = 0;
        for(const T& t : vectorIn)
        {
            arr[i] = t;
            i ++;
        }

        return arr;
    }

    template<typename T, std::size_t Y>
    auto toVector(const std::array<T, Y> &arrIn) noexcept
    {
        std::vector<T> vec;

        for(const T& t : arrIn)
        {
            vec.push_back(t);
        }

        return vec;
    }

    template<typename T, std::size_t Y>
    auto addArray(const std::array<T, Y> &arrIn, const std::vector<T> &vecIn) noexcept
    {
        vecIn.insert(vecIn.end(), arrIn.begin(), arrIn.end());

        return vecIn;
    }

    template<typename T, std::size_t Y>
    auto addVector(const std::vector<T> &vecIn, const std::array<T, Y> &arrIn) noexcept
    {
        std::array<T, Y + vecIn.size()> arr;

        std::uint32_t i = 0;

        for(const T& t : arrIn)
        {
            arr[i] = t;
            i ++;
        }
        for(const T& t : vecIn)
        {
            arr[i] = t;
            i ++;
        }

        return arr;
    }
}
