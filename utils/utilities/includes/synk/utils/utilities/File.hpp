#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <filesystem>

namespace synk::utils::utilities
{
    namespace File
    {
        inline static std::string readFile(const std::filesystem::path& file)
        {
            if (!std::filesystem::exists(file))
            {
                throw std::runtime_error("File : " + file.string() + " not found!");
            }

            auto    file_stream =   std::ifstream(file, std::ios::ate);

            if (!file_stream.is_open())
            {
                throw std::runtime_error("Error while opening the file : " + file.string() + "!");
            }

            const auto  file_size   =   std::size_t(file_stream.tellg());
            auto        buffer  =   std::string {};

            buffer.resize(file_size);

            file_stream.seekg(0);
            file_stream.read(buffer.data(), file_size);

            return buffer;
        }
    };
};
