#pragma once

#include <iostream>
#include <array>
#include <vector>

namespace synk::utils::utilities
{
    namespace Data
    {
        template<typename T>
        auto toArray(const std::vector<T> &vectorIn) noexcept;

        template<typename T, std::size_t Y>
        auto toVector(const std::array<T, Y> &arrIn) noexcept;

        template<typename T, std::size_t Y>
        auto addArray(const std::array<T, Y> &arrIn, const std::vector<T> &vecIn) noexcept;

        template<typename T, std::size_t Y>
        auto addVector(const std::vector<T> &vecIn, const std::array<T, Y> &arrIn) noexcept;
    }
}

#include <synk/utils/utilities/Data.inl>
