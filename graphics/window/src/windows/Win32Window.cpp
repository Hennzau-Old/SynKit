#include <synk/graphics/window/windows/Win32Window.hpp>
#include <synk/utils/logs/Logger.hpp>

#include <synk/utils/utilities/OS.hpp>

#ifdef SYNK_OS_WINDOWS

using namespace synk::graphics::window;
using namespace synk::utils::logs;

static inline constexpr const auto  WINDOW_MODULE   =   "WINDOW";
static inline constexpr const auto  CLASS_NAME      =   L"SynKit_Window";

Win32Window::Win32Window(const std::string& title, const VideoSettings& settings, const WindowStyle& style)
:
    AbstractedWindow(title, settings, style)
{
    Logger::iLog({ WINDOW_MODULE }, "Creating the SynKit_Window with Win32API");

    std::memset(&m_window_class, 0, sizeof(m_window_class));

    m_window_class.lpfnWndProc  =   &Win32Window::proc;
    m_window_class.hInstance    =   GetModuleHandleW(nullptr);
    m_window_class.lpszClassName=   CLASS_NAME;

    if (!RegisterClassW(&m_window_class))
    {
        Logger::fLog({ WINDOW_MODULE }, "Window Registration Class failed!");
        MessageBox(NULL, "Window Registration Class failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
    }
}

Win32Window::~Win32Window()
{
    DestroyWindow(m_window);
    UnregisterClassW(CLASS_NAME, GetModuleHandleW(nullptr));
}

void Win32Window::build()
{
    Logger::iLog({ WINDOW_MODULE }, "Building the SynKit_Window");

    auto window_style       =   DWORD { WS_BORDER | CS_OWNDC };

    if ((m_style & WindowStyle::TitleBar) == WindowStyle::TitleBar)
    {
        window_style |= WS_CAPTION;
    }
    if ((m_style & WindowStyle::Close) == WindowStyle::Close)
    {
        window_style |= WS_SYSMENU;
    }
    if ((m_style & WindowStyle::Minimizable) == WindowStyle::Minimizable)
    {
        window_style |= WS_MINIMIZEBOX;
    }
    if ((m_style & WindowStyle::Resizable) == WindowStyle::Resizable)
    {
        window_style |= WS_MAXIMIZEBOX | WS_THICKFRAME;
    }

    auto window_instance    =   GetModuleHandleA(nullptr);
    auto window_title       =   std::wstring { m_title.begin(), m_title.end() };

    m_window    =   CreateWindowExW(
                        0l,
                        CLASS_NAME,
                        window_title.c_str(),
                        window_style,
                        CW_USEDEFAULT,
                        CW_USEDEFAULT,
                        m_settings.width,
                        m_settings.height,
                        nullptr,
                        nullptr,
                        window_instance,
                        this
                );

    if ((m_style & WindowStyle::FullScreen) == WindowStyle::FullScreen)
    {
        auto mode         = DEVMODE {};
        mode.dmSize       = sizeof(DEVMODE);
        mode.dmBitsPerPel = 0;
        mode.dmPelsWidth  = m_settings.screen.width;
        mode.dmPelsHeight = m_settings.screen.height;
        mode.dmFields     = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

        ChangeDisplaySettings(&mode, CDS_FULLSCREEN);

        SetWindowLongW(m_window, GWL_STYLE, WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
        SetWindowLongW(m_window, GWL_EXSTYLE, WS_EX_APPWINDOW);

        SetWindowPos(m_window,
                     HWND_TOP,
                     m_settings.screen.x,
                     m_settings.screen.y,
                     m_settings.screen.width,
                     m_settings.screen.height,
                     SWP_FRAMECHANGED);

    } else
    {
        SetWindowPos(m_window, 0 , m_settings.screen.x + m_settings.x, m_settings.screen.y + m_settings.y, m_settings.width, m_settings.height, SWP_SHOWWINDOW);
    }

    auto    c_rect   =   RECT {}, w_rect = RECT {}, o_rect = RECT {};

    GetWindowRect   (m_window, &w_rect);
    GetClientRect   (m_window, &c_rect);
    MapWindowPoints (m_window, nullptr, reinterpret_cast<LPPOINT>(&c_rect), 2);

    o_rect.left     = c_rect.left   -   w_rect.left;
    o_rect.top      = c_rect.top    -   w_rect.top;
    o_rect.right    = w_rect.right  -   c_rect.right;
    o_rect.bottom   = w_rect.bottom -   c_rect.bottom;

    m_settings.border_width     =   o_rect.right    +   o_rect.left;
    m_settings.border_height    =   o_rect.top      +   o_rect.bottom;

    ShowWindow(m_window, SW_SHOW);
    UpdateWindow(m_window);

    m_device_context    =   GetDC(m_window);

    m_is_open   =   true;
}

void Win32Window::update()
{
    AbstractedWindow::update();
}

void Win32Window::pollEvents()
{
    while (PeekMessageW(&m_window_message, nullptr, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&m_window_message);
        DispatchMessageW(&m_window_message);
    }
}

void Win32Window::processEvents(UINT msg, WPARAM w_param, LPARAM l_param)
{
    if (!m_window) return;

    switch(msg)
    {
        case WM_CLOSE:
        {
            closeEvent();
            break;
        }

        case WM_SIZE:
        {
            GetWindowRect(m_window, &m_rect);

            m_settings.width    =   m_rect.right    -   m_rect.left;
            m_settings.height   =   m_rect.bottom   -   m_rect.top;

            resizeEvent(m_settings.width, m_settings.height);

            if (w_param == SIZE_MINIMIZED)
            {
                minimizeEvent();
            } else if (w_param == SIZE_MAXIMIZED)
            {
                maximizeEvent();
            }

            break;
        }

        case WM_MOVE:
        {
            GetWindowRect(m_window, &m_rect);

            m_settings.x        =   m_rect.left;
            m_settings.y        =   m_rect.top;

            moveEvent(m_settings.x, m_settings.y);

            break;
        }

        case WM_LBUTTONDOWN:
        {
            buttonPressedEvent(Mouse::Button::Left);

            break;
        }

        case WM_LBUTTONUP:
        {
            buttonReleasedEvent(Mouse::Button::Left);

            break;
        }

        case WM_MBUTTONDOWN:
        {
            buttonPressedEvent(Mouse::Button::Middle);

            break;
        }

        case WM_MBUTTONUP:
        {
            buttonReleasedEvent(Mouse::Button::Middle);

            break;
        }

        case WM_RBUTTONDOWN:
        {
            buttonPressedEvent(Mouse::Button::Right);

            break;
        }

        case WM_RBUTTONUP:
        {
            buttonReleasedEvent(Mouse::Button::Right);

            break;
        }

        case WM_KEYDOWN:
        {
            keyPressedEvent(toRealKey(w_param, l_param));

            break;
        }

        case WM_KEYUP:
        {
            keyReleasedEvent(toRealKey(w_param, l_param));

            break;
        }

        case WM_MOUSEMOVE:
        {
            const auto  x   =   static_cast<std::int16_t> (LOWORD(l_param));
            const auto  y   =   static_cast<std::int16_t> (HIWORD(l_param));

            mouseMovedEvent(x, y);

            if (GetCapture() != m_window)
            {
                SetCapture(m_window);
            }

            GetClientRect(m_window, &m_rect);

            if (x >= m_rect.left && x < m_rect.right && y >= m_rect.top && y < m_rect.bottom)
            {
                if (!m_mouse_in)
                {
                    m_mouse_in  =   true;

                    mouseEnteredEvent();
                }
            } else
            {
                if (m_mouse_in)
                {
                    m_mouse_in  =   false;

                    mouseExitedEvent();

                    if (GetCapture() == m_window)
                    {
                        ReleaseCapture();
                    }
                }
            }

            break;
        }
    }
}

void Win32Window::setTitle(const std::string &title)    noexcept
{
    SetWindowTextA(m_window, title.c_str());

    AbstractedWindow::setTitle(title);
}

void Win32Window::setPos(const std::int16_t &x, const std::int16_t &y)   noexcept
{
    SetWindowPos(m_window, NULL, x, y, getWidth(), getHeight(), SWP_SHOWWINDOW);

    AbstractedWindow::setPos(x, y);
}

LRESULT Win32Window::proc(HWND hwnd, UINT msg, WPARAM w_param, LPARAM l_param)
{
    if (msg == WM_CREATE)
    {
        auto lp_create_params   =   reinterpret_cast<CREATESTRUCT *>(l_param)->lpCreateParams;
        SetWindowLongPtrW(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(lp_create_params));
    }

    auto window =   hwnd ? reinterpret_cast<Win32Window*>(GetWindowLongPtrW(hwnd, GWLP_USERDATA)) : nullptr;

    if (window)
    {
        window->processEvents(msg, w_param, l_param);
    }

    if (msg == WM_CLOSE)
    {
        return 0;
    }

    if ((msg == WM_SYSCOMMAND) && (w_param == SC_KEYMENU))
    {
        return 0;
    }

    return DefWindowProcW(hwnd, msg, w_param, l_param);
}

#endif
