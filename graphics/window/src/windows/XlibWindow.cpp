#include <synk/graphics/window/windows/XlibWindow.hpp>
#include <synk/utils/logs/Logger.hpp>

#ifdef SYNK_OS_LINUX

using namespace synk::graphics::window;
using namespace synk::utils::logs;

static inline constexpr const auto  WINDOW_MODULE   =   "WINDOW";

XlibWindow::XlibWindow(const std::string& title, const AbstractedWindow::VideoSettings& settings, const AbstractedWindow::WindowStyle& style)
:
    AbstractedWindow(title, settings, style)
{
    Logger::iLog({ WINDOW_MODULE }, "Creating the SynKit_Window with Xlib");

    m_display   =   XOpenDisplay(NULL);

    if (m_display == NULL)
    {
        Logger::fLog({WINDOW_MODULE}, "XOpenDisplay failed!");
        return;
    }

    m_screen = DefaultScreen(m_display);

    m_visual_info.depth     =   XDefaultDepth(m_display, m_screen);
    m_visual_info.visual    =   XDefaultVisual(m_display, m_screen);

    m_attributes.border_pixel       = BlackPixel(m_display, m_screen);
    m_attributes.background_pixel   = WhitePixel(m_display, m_screen);
    m_attributes.override_redirect  = True;
    m_attributes.colormap           = XCreateColormap(m_display, RootWindow(m_display, m_screen), m_visual_info.visual, AllocNone);
    m_attributes.event_mask         = ExposureMask;
}

XlibWindow::~XlibWindow()
{
    XAutoRepeatOn(m_display);
    XCloseDisplay(m_display);
}

void XlibWindow::build()
{
    m_window = XCreateWindow    (m_display,
                                 RootWindow(m_display, m_screen),
                                 m_settings.x + m_settings.screen.x,
                                 m_settings.y + m_settings.screen.y,
                                 m_settings.width,
                                 m_settings.height,
                                 0,
                                 m_visual_info.depth,
                                 InputOutput,
                                 m_visual_info.visual,
                                 CWBackPixel | CWColormap | CWBorderPixel | CWEventMask,
                                 &m_attributes);

    if ((m_style & WindowStyle::FullScreen) == WindowStyle::FullScreen)
    {
        const auto  wm_state            =   XInternAtom(m_display, "_NET_WM_STATE", false);
        const auto  wm_state_fullscreen =   XInternAtom(m_display, "_NET_WM_STATE_FULLSCREEN", false);

        XChangeProperty(m_display,
                        m_window,
                        wm_state,
                        XA_ATOM,
                        32,
                        PropModeReplace,
                        reinterpret_cast<const unsigned char*>(&wm_state_fullscreen),
                        1);
    } else
    {
        const auto MWM_HINTS_FUNCTIONS   = 1 << 0;
        const auto MWM_HINTS_DECORATIONS = 1 << 1;

        const auto MWM_DECOR_BORDER   = 1 << 1;
        const auto MWM_DECOR_RESIZE   = 1 << 2;
        const auto MWM_DECOR_TITLE    = 1 << 3;
        const auto MWM_DECOR_MENU     = 1 << 4;
        const auto MWM_DECOR_MINIMIZE = 1 << 5;
        const auto MWM_DECOR_MAXIMIZE = 1 << 6;

        const auto MWM_FUNC_RESIZE   = 1 << 1;
        const auto MWM_FUNC_MOVE     = 1 << 2;
        const auto MWM_FUNC_MINIMIZE = 1 << 3;
        const auto MWM_FUNC_MAXIMIZE = 1 << 4;
        const auto MWM_FUNC_CLOSE    = 1 << 5;

        struct  Hints
        {
            std::uint32_t   flags       = MWM_HINTS_FUNCTIONS | MWM_HINTS_DECORATIONS;
            std::uint32_t   functions   = 0;
            std::uint32_t   decorations = 0;
            std::int32_t    input_mode  = 0;
            std::uint32_t   state       = 0;
        };

        auto    hints   =   Hints   {};

        if ((m_style & WindowStyle::TitleBar) == WindowStyle::TitleBar)
        {
            hints.decorations |= MWM_DECOR_BORDER | MWM_DECOR_TITLE | MWM_DECOR_MENU;
            hints.functions |= MWM_FUNC_MOVE;
        }

        if ((m_style & WindowStyle::Close) == WindowStyle::Minimizable)
        {
            hints.decorations |= 0;
            hints.functions |= MWM_FUNC_CLOSE;
        }

        if ((m_style & WindowStyle::Minimizable) == WindowStyle::Minimizable)
        {
            hints.decorations |= MWM_DECOR_MINIMIZE;
            hints.functions |= MWM_FUNC_MINIMIZE;
        }

        if ((m_style & WindowStyle::Resizable) == WindowStyle::Resizable)
        {
            hints.decorations |= MWM_DECOR_RESIZE | MWM_DECOR_MAXIMIZE;
            hints.functions |= MWM_FUNC_RESIZE | MWM_FUNC_MAXIMIZE;
        }

        const auto  properties  =   XInternAtom(m_display, "_MOTIF_WM_HINTS", true);

        XChangeProperty(m_display,
                        m_window,
                        properties,
                        properties,
                        32,
                        PropModeReplace,
                        reinterpret_cast<const unsigned char*>(&hints),
                        1);

        if ((m_style & WindowStyle::Resizable) != WindowStyle::Resizable)
        {
            auto    size_hints      =   XAllocSizeHints();
            size_hints->flags       =   PMinSize | PMaxSize;
            size_hints->max_width   =   size_hints->min_width   =   m_settings.width;
            size_hints->max_height  =   size_hints->max_height  =   m_settings.height;

            XSetWMNormalHints(m_display, m_window, size_hints);

            free(size_hints);
         }
    }

    XSetErrorHandler(errorHandler);
    XStoreName(m_display, m_window, m_title.c_str());
    XSelectInput(m_display, m_window,   FocusChangeMask | ButtonPressMask | ButtonReleaseMask |
                                        ButtonMotionMask | PointerMotionMask | KeyPressMask |
                                        KeyReleaseMask | StructureNotifyMask | EnterWindowMask |
                                        LeaveWindowMask | VisibilityChangeMask | PropertyChangeMask |
                                        ExposureMask);
    XMapWindow(m_display, m_window);
    XAutoRepeatOff(m_display);

    m_is_open   =   true;
}

int XlibWindow::errorHandler(Display *display, XErrorEvent *e)
{
    return 0;
}

void XlibWindow::update()
{
    AbstractedWindow::update();
}

void XlibWindow::pollEvents()
{
    XEventsQueued(m_display, QueuedAlready);
    const auto  number  =   XPending(m_display);

    if (number > 0)
    {
        XNextEvent(m_display, &m_event);

        switch(m_event.type)
        {
            case DestroyNotify:
            {
                closeEvent();
                break;
            }

            case ConfigureNotify:
            {
                if (m_event.xconfigure.width && m_event.xconfigure.height)
                {
                    if (m_settings.width != m_event.xconfigure.width ||
                        m_settings.height != m_event.xconfigure.height)
                    {
                        resizeEvent(m_event.xconfigure.width, m_event.xconfigure.height);
                    }
                }

                if (m_event.xconfigure.x && m_event.xconfigure.y)
                {
                    if (m_settings.x != m_event.xconfigure.x ||
                        m_settings.y != m_event.xconfigure.y)
                    {
                        moveEvent(m_event.xconfigure.x, m_event.xconfigure.y);
                    }
                }

                break;
            }

            case KeyPress:
            {
                int keysyms_per_keycode_return;
                KeySym *keysym = XGetKeyboardMapping(m_display,
                    m_event.xkey.keycode,
                    1,
                    &keysyms_per_keycode_return);

                keyPressedEvent(toRealKey(keysym[0]));

                XFree(keysym);

                break;
            }

            case KeyRelease:
            {
                int keysyms_per_keycode_return;
                KeySym *keysym = XGetKeyboardMapping(m_display,
                    m_event.xkey.keycode,
                    1,
                    &keysyms_per_keycode_return);

                keyReleasedEvent(toRealKey(keysym[0]));

                XFree(keysym);

                break;
            }

            case ButtonPress:
            {
                buttonPressedEvent(toRealButton(m_event.xbutton.button));

                break;
            }

            case ButtonRelease:
            {
                buttonReleasedEvent(toRealButton(m_event.xbutton.button));

                break;
            }

            case MotionNotify:
            {
                mouseMovedEvent(m_event.xmotion.x, m_event.xmotion.y);
                break;
            }

            case EnterNotify:
            {
                mouseEnteredEvent();
                break;
            }

            case LeaveNotify:
            {
                mouseExitedEvent();
                break;
            }
        }
    }
}

void XlibWindow::setTitle(const std::string &title)    noexcept
{
    XStoreName(m_display, m_window, title.c_str());

    AbstractedWindow::setTitle(title);
}

void XlibWindow::setPos(const std::int16_t &x, const std::int16_t &y)   noexcept
{
    XMoveWindow(m_display, m_window, x, y);

    AbstractedWindow::setPos(x, y);
}

#endif
