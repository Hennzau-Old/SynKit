#include <synk/graphics/window/Screens.hpp>

using namespace synk::graphics::window;

#ifdef SYNK_OS_WINDOWS

std::vector<Screens::Screen>    Screens::enumerateScreens()
{
    auto results = std::vector<Screens::Screen> {};

    EnumDisplayMonitors(0, 0, enumerateMonitors, reinterpret_cast<LPARAM>(&results));

    return results;
}

BOOL Screens::enumerateMonitors(HMONITOR monitor, HDC hdc, LPRECT monitor_rect, LPARAM pData)
{
    auto    results =   reinterpret_cast<std::vector<Screens::Screen>*>(pData);

    auto    result  =   Screens::Screen {
            .x      =   static_cast<std::int16_t>(monitor_rect->left),
            .y      =   static_cast<std::int16_t>(monitor_rect->top),
            .width  =   static_cast<std::uint16_t>(monitor_rect->right - monitor_rect->left),
            .height =   static_cast<std::uint16_t>(monitor_rect->bottom - monitor_rect->top)
    };

    results->push_back(result);

    return TRUE;
}

#elif SYNK_OS_LINUX

#include <xcb/randr.h>
#include <xcb/xcb.h>

std::vector<Screens::Screen>    Screens::enumerateScreens()
{
    auto results    =   std::vector<Screens::Screen> {};

    auto display = xcb_connect(nullptr, nullptr);
    auto screen  = xcb_setup_roots_iterator(xcb_get_setup(display)).data;
    auto root    = screen->root;

    auto reply = xcb_randr_get_screen_resources_current_reply(display, xcb_randr_get_screen_resources_current(display, root), nullptr);

    auto timestamp = reply->config_timestamp;

    auto len          = xcb_randr_get_screen_resources_current_outputs_length(reply);
    auto randr_output = xcb_randr_get_screen_resources_current_outputs(reply);

    results.reserve(len);

    for (auto i = 0; i < len; ++i)
    {
        auto info   =   xcb_randr_get_output_info(display, randr_output[i], timestamp);
        auto output =   xcb_randr_get_output_info_reply(display, info, nullptr);

        if (output == nullptr)
        {
            continue;
        }

        auto crtc = xcb_randr_get_crtc_info_reply(display, xcb_randr_get_crtc_info(display, output->crtc, timestamp), nullptr);

        if (crtc == nullptr)
        {
            continue;
        }

        auto video_setting = Screens::Screen {
                .x      = static_cast<std::int16_t> (crtc->x),
                .y      = static_cast<std::int16_t> (crtc->y),
                .width  = static_cast<std::uint16_t> (crtc->width),
                .height = static_cast<std::uint16_t> (crtc->height)
        };

        results.emplace_back(std::move(video_setting));

        free(crtc);
        free(output);
    }

    free(reply);

    xcb_disconnect(display);

    return results;
}

#endif
