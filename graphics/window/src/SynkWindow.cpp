#include <synk/graphics/window/SynkWindow.hpp>

#ifdef SYNK_OS_WINDOWS

    #include <synk/graphics/window/windows/Win32Window.hpp>

#elif SYNK_OS_LINUX

    #include <synk/graphics/window/windows/XlibWindow.hpp>

#endif

using namespace synk::graphics::window;

SynkWindow::SynkWindow(const std::string& title, const AbstractedWindow::VideoSettings& settings, const AbstractedWindow::WindowStyle& style)
{
    #ifdef SYNK_OS_WINDOWS

        m_window    =   std::make_unique<Win32Window>   (title, settings, style);

    #elif SYNK_OS_LINUX

        m_window    =   std::make_unique<XlibWindow>    (title, settings, style);

    #endif

    m_event_handler =   std::make_unique<EventHandler> ();
}

SynkWindow::~SynkWindow()
{

}

