#include <synk/graphics/window/handlers/InputHandler.hpp>

using namespace synk::graphics::window;

InputHandler::InputHandler()
{
    for (auto i = 0u; i < m_event_keys.size(); i++)
    {
        m_event_keys[i]     =   InputStatus::Up;

        m_current_keys[i]   =   false;
        m_down_keys[i]      =   false;
        m_up_keys[i]        =   false;
    }
}

InputHandler::~InputHandler()
{

}

void InputHandler::update() noexcept
{
    for (auto i = 0u; i < static_cast<int> (Keyboard::Key::NumKeys); i++)
    {
        m_down_keys[i]    =   false;

        if (isKeyRepeat(static_cast<Keyboard::Key> (i)) && !m_current_keys[i])
        {
            m_down_keys[i]    = true;
        }

        m_up_keys[i]    =   false;

        if (!isKeyRepeat(static_cast<Keyboard::Key> (i)) && m_current_keys[i])
        {
            m_up_keys[i]    =   true;
        }

        m_current_keys[i]   =   false;

        if (isKeyRepeat(static_cast<Keyboard::Key> (i)))
        {
            m_current_keys[i]   =   true;
        }
    }

    for (auto i = 0u; i < static_cast<int>(Mouse::Button::NumButtons); i++)
    {
        m_down_buttons[i]   =   false;

        if (isButtonRepeat(static_cast<Mouse::Button> (i)) && !m_current_buttons[i])
        {
            m_down_buttons[i]   =   true;
        }

        m_up_buttons[i] =   false;

        if (!isButtonRepeat(static_cast<Mouse::Button> (i)) && m_current_buttons[i])
        {
            m_up_buttons[i] =   true;
        }

        m_current_buttons[i]    =   false;

        if (isButtonRepeat(static_cast<Mouse::Button> (i)))
        {
            m_current_buttons[i]    =   true;
        }
    }
}

void InputHandler::keyRepeatEvent(std::queue<EventHandler::Event> &queue)    const   noexcept
{
    auto  event   =   EventHandler::Event
    {
        .type   =   EventHandler::EventType::KeyRepeat,
    };

    auto    num =   0u;
    for (const auto&    v   :   m_event_keys)
    {
        if (v == InputStatus::Down)
        {
            event.key   =   static_cast<Keyboard::Key>(num);

            queue.push(event);
        }

        num++;
    }
}

void InputHandler::buttonRepeatEvent(std::queue<EventHandler::Event> &queue)    const   noexcept
{
    auto    event   =   EventHandler::Event
    {
            .type   =   EventHandler::EventType::MouseButtonRepeat
    };

    auto    num =   0u;
    for (const auto&    v   :   m_event_buttons)
    {
        if (v   ==  InputStatus::Down)
        {
            event.button    =   static_cast<Mouse::Button>(num);

            queue.push(event);
        }

        num++;
    }
}
