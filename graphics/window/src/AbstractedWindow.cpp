#include <synk/graphics/window/AbstractedWindow.hpp>

#include <synk/graphics/window/handlers/EventHandler.hpp>
#include <synk/graphics/window/handlers/InputHandler.hpp>

using namespace synk::graphics::window;

AbstractedWindow::AbstractedWindow(const std::string& title, const VideoSettings& settings, const WindowStyle& window_style)
:
    m_title         { title },
    m_style         { window_style },
    m_settings      { settings }
{
    m_input_handler =   std::make_unique<InputHandler> ();
}

AbstractedWindow::~AbstractedWindow()
{

}
