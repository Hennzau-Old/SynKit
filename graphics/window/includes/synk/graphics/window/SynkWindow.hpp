#pragma once

#include <iostream>
#include <string>

#include <synk/utils/utilities/NonCopyable.hpp>
#include <synk/utils/utilities/OS.hpp>

#include <synk/graphics/window/Fwd.hpp>
#include <synk/graphics/window/AbstractedWindow.hpp>

#include <synk/graphics/window/handlers/Fwd.hpp>
#include <synk/graphics/window/handlers/EventHandler.hpp>

#include <synk/graphics/window/windows/Fwd.hpp>

namespace synk::graphics::window
{
    class SynkWindow    :   public synk::utils::utilities::NonCopyable
    {
        public:

            SynkWindow  (const std::string& title, const AbstractedWindow::VideoSettings& settings, const AbstractedWindow::WindowStyle& style);
            ~SynkWindow ();

            static inline auto  open    (const std::string& title, const AbstractedWindow::VideoSettings& settings, const AbstractedWindow::WindowStyle& style) noexcept;

            template<typename T>
            inline auto         extract()   noexcept
            {
                return std::make_unique<T> (*this);
            }

            inline auto     build       ();
            inline auto     update      ();
            inline auto     pollEvents  ();

            inline auto     setTitle    (const std::string& title)                      noexcept;
            inline auto     setPos      (const std::int16_t& x, const std::int16_t& y)  noexcept;

            // Getters

            inline auto&    isOpen          ()  const   noexcept    { return m_window->isOpen();            }
            inline auto&    getTitle        ()  const   noexcept    { return m_window->getTitle();          }
            inline auto&    getPosX         ()  const   noexcept    { return m_window->getPosX();           }
            inline auto&    getPosY         ()  const   noexcept    { return m_window->getPosY();           }
            inline auto&    getWidth        ()  const   noexcept    { return m_window->getWidth();          }
            inline auto&    getHeight       ()  const   noexcept    { return m_window->getHeight();         }
            inline auto&    getBorderWidth  ()  const   noexcept    { return m_window->getBorderWidth();    }
            inline auto&    getBorderHeight ()  const   noexcept    { return m_window->getBorderHeight();   }

            inline auto&    getStyle        ()  const   noexcept    { return m_window->getStyle();          }
            inline auto&    getVideoSettings()  const   noexcept    { return m_window->getVideoSettings();  }

            inline auto&    getAbstracted   ()          noexcept    { return m_window;  }
            inline auto&    getAbstracted   ()  const   noexcept    { return m_window;  }

            // Handlers

            inline auto     getInputHandler ()          noexcept    { return m_window->getInputHandler();   }
            inline auto     getInputHandler ()  const   noexcept    { return m_window->getInputHandler();   }

            inline auto     getEventHandler ()          noexcept    { return synk::utils::utilities::makeObserver(m_event_handler);         }
            inline auto     getEventHandler ()  const   noexcept    { return synk::utils::utilities::makeConstObserver(m_event_handler);    }

            using WindowStyle   =   AbstractedWindow::WindowStyle;
            using VideoSettings =   AbstractedWindow::VideoSettings;
            using Event         =   EventHandler::Event;
            using EventType     =   EventHandler::EventType;
            using Key           =   Keyboard::Key;
            using Button        =   Mouse::Button;

        private:

            AbstractedWindowOwnedPtr    m_window;
            EventHandlerOwnedPtr        m_event_handler;

            bool                        m_has_been_built    =   false;
    };
}

#include <synk/graphics/window/SynkWindow.inl>
