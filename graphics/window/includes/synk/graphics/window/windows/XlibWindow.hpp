#pragma once

#include <synk/utils/utilities/OS.hpp>

#include <synk/graphics/window/AbstractedWindow.hpp>

#ifdef SYNK_OS_LINUX

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

namespace synk::graphics::window
{
    class XlibWindow : public AbstractedWindow
    {
        public:

            XlibWindow  (const std::string& title, const VideoSettings& settings, const WindowStyle& style);
            virtual ~XlibWindow();

            virtual void        build       ();
            virtual void        update      ();
            virtual void        pollEvents  ();

            virtual void        setTitle    (const std::string& title)                      noexcept;
            virtual void        setPos      (const std::int16_t& x, const std::int16_t& y)  noexcept;

            inline auto         toRealKey   (KeySym key)        const   noexcept;
            inline auto         toRealButton(const int& button) const   noexcept;

            inline auto         getDisplay  ()          noexcept    {   return  m_display;  }
            inline auto         getDisplay  ()  const   noexcept    {   return  m_display;  }

            inline auto&        getWindow   ()          noexcept    {   return  m_window;   }
            inline auto&        getWindow   ()  const   noexcept    {   return  m_window;   }

            inline auto&        getScreen   ()          noexcept    {   return  m_screen;   }
            inline auto&        getScreen   ()  const   noexcept    {   return  m_screen;   }

            inline auto&        getVisualInfo   ()          noexcept    {   return m_visual_info;   }
            inline auto&        getVisualInfo   ()  const   noexcept    {   return m_visual_info;   }

            inline auto&        getAttributes   ()          noexcept    {   return  m_attributes;   }
            inline auto&        getAttributes   ()  const   noexcept    {   return  m_attributes;   }

        private:

            static int          errorHandler(Display* display, XErrorEvent* e);

            Window              m_window;
            Display*            m_display;
            int                 m_screen;
            XEvent              m_event;

            XVisualInfo             m_visual_info;
            XSetWindowAttributes    m_attributes;
    };
}

#include <synk/graphics/window/windows/XlibWindow.inl>

#endif
