#pragma once

#include <synk/utils/utilities/Memory.hpp>

namespace synk::gaphics::window
{
    class Win32Window;
    DECLARE_PTR_AND_REF(Win32Window)

    class XlibWindow;
    DECLARE_PTR_AND_REF(XlibWindow)
}
