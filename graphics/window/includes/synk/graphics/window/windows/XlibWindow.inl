#pragma once

#include <synk/graphics/window/windows/XlibWindow.hpp>

namespace synk::graphics::window
{
    inline auto XlibWindow::toRealKey(KeySym key)  const   noexcept
    {
        switch (key)
        {
            case XK_a:              return Keyboard::Key::A;
            case XK_b:              return Keyboard::Key::B;
            case XK_c:              return Keyboard::Key::C;
            case XK_d:              return Keyboard::Key::D;
            case XK_e:              return Keyboard::Key::E;
            case XK_f:              return Keyboard::Key::F;
            case XK_g:              return Keyboard::Key::G;
            case XK_h:              return Keyboard::Key::H;
            case XK_i:              return Keyboard::Key::I;
            case XK_j:              return Keyboard::Key::J;
            case XK_k:              return Keyboard::Key::K;
            case XK_l:              return Keyboard::Key::L;
            case XK_m:              return Keyboard::Key::M;
            case XK_n:              return Keyboard::Key::N;
            case XK_o:              return Keyboard::Key::O;
            case XK_p:              return Keyboard::Key::P;
            case XK_q:              return Keyboard::Key::Q;
            case XK_r:              return Keyboard::Key::R;
            case XK_s:              return Keyboard::Key::S;
            case XK_t:              return Keyboard::Key::T;
            case XK_u:              return Keyboard::Key::U;
            case XK_v:              return Keyboard::Key::V;
            case XK_w:              return Keyboard::Key::W;
            case XK_x:              return Keyboard::Key::X;
            case XK_y:              return Keyboard::Key::Y;
            case XK_z:              return Keyboard::Key::Z;
            case XK_0:              return Keyboard::Key::Num0;
            case XK_1:              return Keyboard::Key::Num1;
            case XK_2:              return Keyboard::Key::Num2;
            case XK_3:              return Keyboard::Key::Num3;
            case XK_4:              return Keyboard::Key::Num4;
            case XK_5:              return Keyboard::Key::Num5;
            case XK_6:              return Keyboard::Key::Num6;
            case XK_7:              return Keyboard::Key::Num7;
            case XK_8:              return Keyboard::Key::Num8;
            case XK_9:              return Keyboard::Key::Num9;
            case XK_KP_0:           return Keyboard::Key::Numpad0;
            case XK_KP_1:           return Keyboard::Key::Numpad1;
            case XK_KP_2:           return Keyboard::Key::Numpad2;
            case XK_KP_3:           return Keyboard::Key::Numpad3;
            case XK_KP_4:           return Keyboard::Key::Numpad4;
            case XK_KP_5:           return Keyboard::Key::Numpad5;
            case XK_KP_6:           return Keyboard::Key::Numpad6;
            case XK_KP_7:           return Keyboard::Key::Numpad7;
            case XK_KP_8:           return Keyboard::Key::Numpad8;
            case XK_KP_9:           return Keyboard::Key::Numpad9;
            case XK_F1:             return Keyboard::Key::F1;
            case XK_F2:             return Keyboard::Key::F2;
            case XK_F3:             return Keyboard::Key::F3;
            case XK_F4:             return Keyboard::Key::F4;
            case XK_F5:             return Keyboard::Key::F5;
            case XK_F6:             return Keyboard::Key::F6;
            case XK_F7:             return Keyboard::Key::F7;
            case XK_F8:             return Keyboard::Key::F8;
            case XK_F9:             return Keyboard::Key::F9;
            case XK_F10:            return Keyboard::Key::F10;
            case XK_F11:            return Keyboard::Key::F11;
            case XK_F12:            return Keyboard::Key::F12;
            case XK_F13:            return Keyboard::Key::F13;
            case XK_F14:            return Keyboard::Key::F14;
            case XK_F15:            return Keyboard::Key::F15;
            case XK_Control_L:      return Keyboard::Key::LeftControl;
            case XK_Shift_L:        return Keyboard::Key::LeftShift;
            case XK_Alt_L:          return Keyboard::Key::LeftAlt;
            case XK_Super_L:        return Keyboard::Key::LeftSystem;
            case XK_Control_R:      return Keyboard::Key::RightControl;
            case XK_Shift_R:        return Keyboard::Key::RightShift;
            case XK_Alt_R:          return Keyboard::Key::RightAlt;
            case XK_Super_R:        return Keyboard::Key::RightSystem;
            case XK_bracketleft:    return Keyboard::Key::LeftBracket;
            case XK_bracketright:   return Keyboard::Key::RightBracket;
            case XK_Escape:         return Keyboard::Key::Escape;
            case XK_space:          return Keyboard::Key::Space;
            case XK_BackSpace:      return Keyboard::Key::BackSpace;
            case XK_Return:         return Keyboard::Key::Enter;
            case XK_Menu:           return Keyboard::Key::Menu;
            case XK_semicolon:      return Keyboard::Key::SemiColon;
            case XK_comma:          return Keyboard::Key::Comma;
            case XK_period:         return Keyboard::Key::Period;
            case XK_quoteright:     [[fallthrough]];
            case XK_quoteleft:      return Keyboard::Key::Quote;
            case XK_slash:          return Keyboard::Key::Slash;
            case XK_backslash:      return Keyboard::Key::BackSlash;
            case XK_dead_grave:     return Keyboard::Key::Tilde;
            case XK_equal:          return Keyboard::Key::Equal;
            case XK_hyphen:         return Keyboard::Key::Hyphen;
            case XK_Tab:            return Keyboard::Key::Tab;
            case XK_Page_Up:        return Keyboard::Key::PageUp;
            case XK_Page_Down:      return Keyboard::Key::PageDown;
            case XK_Begin:          return Keyboard::Key::Begin;
            case XK_End:            return Keyboard::Key::End;
            case XK_Home:           return Keyboard::Key::Home;
            case XK_Insert:         return Keyboard::Key::Insert;
            case XK_Delete:         return Keyboard::Key::Delete;
            case XK_KP_Add:         return Keyboard::Key::Add;
            case XK_KP_Subtract:    return Keyboard::Key::Substract;
            case XK_KP_Multiply:    return Keyboard::Key::Multiply;
            case XK_KP_Divide:      return Keyboard::Key::Divide;
            case XK_Left:           return Keyboard::Key::Left;
            case XK_Right:          return Keyboard::Key::Right;
            case XK_Up:             return Keyboard::Key::Up;
            case XK_Down:           return Keyboard::Key::Down;
            default:                return Keyboard::Key::None;
        }
    }

    inline auto XlibWindow::toRealButton(const int& button)    const   noexcept
    {
        using namespace synk::graphics::window;

        switch(button)
        {
            case Button1:   return  Mouse::Button::Left;
            case Button2:   return  Mouse::Button::Middle;
            case Button3:   return  Mouse::Button::Right;
            default:        return  Mouse::Button::None;
        }
    }
}
