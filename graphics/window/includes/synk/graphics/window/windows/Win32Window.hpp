#pragma once

#include <synk/utils/utilities/OS.hpp>

#include <synk/graphics/window/AbstractedWindow.hpp>

#ifdef SYNK_OS_WINDOWS

#include <windows.h>

namespace synk::graphics::window
{
    class Win32Window : public AbstractedWindow
    {
        public:

            Win32Window (const std::string& title, const VideoSettings& settings, const WindowStyle& style);
            virtual ~Win32Window();

            virtual void    build       ();
            virtual void    update      ();
            virtual void    pollEvents  ();

            virtual void    setTitle    (const std::string& title)                      noexcept;
            virtual void    setPos      (const std::int16_t& x, const std::int16_t& y)  noexcept;

            inline auto     toRealKey   (WPARAM w_param, LPARAM l_param)    const   noexcept;

            inline auto&    getHWND     ()  const   noexcept    {   return  m_window;   }
            inline auto&    getDC       ()  const   noexcept    {   return  m_device_context;   }

        private:

            void                        processEvents   (UINT msg, WPARAM w_param, LPARAM l_param);
            static  LRESULT CALLBACK    proc            (HWND hwnd, UINT msg, WPARAM w_param, LPARAM l_param);

            WNDCLASSW       m_window_class;
            MSG             m_window_message;
            HWND            m_window;
            HDC             m_device_context;

            RECT            m_rect;
    };
}

#include <synk/graphics/window/windows/Win32Window.inl>

#endif
