#pragma once

#include <iostream>
#include <vector>

#include <synk/utils/utilities/OS.hpp>

#ifdef SYNK_OS_WINDOWS

#include <windows.h>

#elif SYNK_OS_LINUX

#include <X11/Xlib.h>

#endif

namespace synk::graphics::window
{
    class Screens
    {
        public:

            struct Screen
            {
                std::int16_t   x;
                std::int16_t   y;

                std::uint16_t   width;
                std::uint16_t   height;
            };

            static std::vector<Screen>  enumerateScreens    ();

        private:

            #ifdef SYNK_OS_WINDOWS

                static BOOL CALLBACK    enumerateMonitors(HMONITOR monitor, HDC hdc, LPRECT monitor_rect, LPARAM pData);

            #endif
    };
}
