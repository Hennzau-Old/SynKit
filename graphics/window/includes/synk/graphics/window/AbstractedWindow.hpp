#pragma once

#include <iostream>
#include <string>
#include <queue>

#include <synk/utils/utilities/NonCopyable.hpp>

#include <synk/graphics/window/Screens.hpp>
#include <synk/graphics/window/handlers/Fwd.hpp>
#include <synk/graphics/window/handlers/EventHandler.hpp>
#include <synk/graphics/window/handlers/InputHandler.hpp>

namespace synk::graphics::window
{
    class AbstractedWindow : public synk::utils::utilities::NonCopyable
    {
        public:

            enum class WindowStyle
            {
                TitleBar    =   0b1,
                Close       =   TitleBar | 0b10,
                Minimizable =   TitleBar | 0b100,
                Resizable   =   TitleBar | 0b1000,
                All         =   Resizable | Minimizable | Close,
                FullScreen  =   0b10000
            };

            struct VideoSettings
            {
                std::int16_t    x       =   0;
                std::int16_t    y       =   0;

                std::uint16_t   width   =   0;
                std::uint16_t   height  =   0;

                std::uint16_t   border_width    =   0;
                std::uint16_t   border_height   =   0;

                Screens::Screen screen;
            };

            AbstractedWindow            (const std::string& title, const VideoSettings& settings, const WindowStyle& window_style);
            virtual ~AbstractedWindow   ();

            virtual void build          ()  {}

            inline constexpr friend WindowStyle operator& (const WindowStyle& s1, const WindowStyle& s2)
            {
                return static_cast<WindowStyle>(static_cast<int>(s1) & static_cast<int>(s2));
            }

            inline constexpr friend WindowStyle operator| (const WindowStyle& s1, const WindowStyle& s2)
            {
                return static_cast<WindowStyle>(static_cast<int>(s1) | static_cast<int>(s2));
            }

            // Virtual functions

            virtual void            update          ()  { m_input_handler->update(); keyRepeatEvent(); buttonRepeatEvent(); }
            virtual void            pollEvents      ()  {}

            // Events managing

            inline auto             closeEvent      ()  noexcept;
            inline auto             resizeEvent     (const std::uint16_t& width, const std::uint16_t& height)  noexcept;
            inline auto             moveEvent       (const std::int16_t& x, const std::int16_t& y)  noexcept;
            inline auto             minimizeEvent   ()  noexcept;
            inline auto             maximizeEvent   ()  noexcept;

            inline auto             keyPressedEvent (const Keyboard::Key& key)  noexcept;
            inline void             keyRepeatEvent  ()  noexcept;
            inline auto             keyReleasedEvent(const Keyboard::Key& key)  noexcept;

            inline auto             buttonPressedEvent  (const Mouse::Button& button)  noexcept;
            inline void             buttonRepeatEvent   ()  noexcept;
            inline auto             buttonReleasedEvent (const Mouse::Button& button)  noexcept;

            inline auto             mouseEnteredEvent   ()  noexcept;
            inline auto             mouseExitedEvent    ()  noexcept;
            inline auto             mouseMovedEvent     (const std::int16_t& x, const std::int16_t& y)  noexcept;

            // Setters

            virtual void            setTitle        (const std::string& title)                          noexcept    { m_title = title; }
            virtual void            setPos          (const std::int16_t& x, const std::int16_t& y)      noexcept    { m_settings.x = x; m_settings.y = y; }

            // Getters

            inline auto&            isOpen          ()  const   noexcept    { return m_is_open; }

            inline auto&            getTitle        ()  const   noexcept    { return m_title;                   }
            inline auto&            getPosX         ()  const   noexcept    { return m_settings.x;              }
            inline auto&            getPosY         ()  const   noexcept    { return m_settings.y;              }
            inline auto&            getWidth        ()  const   noexcept    { return m_settings.width;          }
            inline auto&            getHeight       ()  const   noexcept    { return m_settings.height;         }
            inline auto&            getBorderWidth  ()  const   noexcept    { return m_settings.border_width;   }
            inline auto&            getBorderHeight ()  const   noexcept    { return m_settings.border_height;  }

            inline auto&            getStyle        ()  const   noexcept    { return m_style;       }
            inline auto&            getVideoSettings()  const   noexcept    { return m_settings;    }

            inline auto&            getEvents       ()          noexcept    { return m_events; }
            inline auto&            getEvents       ()  const   noexcept    { return m_events; }

            inline auto             getInputHandler ()          noexcept    { return synk::utils::utilities::makeObserver(m_input_handler);         }
            inline auto             getInputHandler ()  const   noexcept    { return synk::utils::utilities::makeConstObserver(m_input_handler);    }

        protected:

            WindowStyle         m_style;
            VideoSettings       m_settings;
            std::string         m_title;

            bool                m_is_open   =   false;
            bool                m_mouse_in  =   false;

            std::queue<EventHandler::Event> m_events;
            InputHandlerOwnedPtr            m_input_handler;
    };
}

#include <synk/graphics/window/AbstractedWindow.inl>
