#pragma once

#include <synk/graphics/window/SynkWindow.hpp>

namespace synk::graphics::window
{
    inline auto SynkWindow::open(const std::string &title, const AbstractedWindow::VideoSettings &settings, const AbstractedWindow::WindowStyle &style)  noexcept
    {
        return std::make_unique<SynkWindow> (title, settings, style);
    }

    inline auto SynkWindow::build()
    {
        if (!m_has_been_built)
        {
            m_window->build();

            m_has_been_built    =   true;
        }
    }

    inline auto SynkWindow::update()
    {
        m_window->update();
    }

    inline auto SynkWindow::pollEvents()
    {
        m_window->pollEvents();

        auto&       events  =   m_window->getEvents();

        while (!events.empty())
        {
            const auto& event   =   events.front();

            m_event_handler->process(event);

            events.pop();
        }
    }

    inline auto SynkWindow::setTitle(const std::string &title) noexcept
    {
        m_window->setTitle(title);
    }

    inline auto SynkWindow::setPos(const std::int16_t &x, const std::int16_t &y)    noexcept
    {
        m_window->setPos(x, y);
    }
}
