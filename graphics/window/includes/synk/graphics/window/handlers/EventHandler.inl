#pragma once

#include <synk/graphics/window/handlers/EventHandler.hpp>

namespace synk::graphics::window
{
    inline auto     EventHandler::add     (const EventType&   type,   const callback& function)   noexcept
    {
        m_callbacks[type].emplace_back(function);
    }

    inline auto     EventHandler::process (const Event& event) noexcept
    {
        const auto& callbacks   =   m_callbacks[event.type];

        for (const auto&    function    :   callbacks)
        {
            function(event);
        }
    }
}
