#pragma once

#include <synk/utils/utilities/Memory.hpp>

namespace synk::graphics::window
{
    class EventHandler;
    DECLARE_PTR_AND_REF(EventHandler)

    class InputHandler;
    DECLARE_PTR_AND_REF(InputHandler)
}
