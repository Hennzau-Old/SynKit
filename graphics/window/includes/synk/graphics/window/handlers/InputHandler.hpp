#pragma once

#include <iostream>
#include <array>
#include <queue>

#include <synk/utils/utilities/NonCopyable.hpp>

#include <synk/graphics/window/handlers/Keys.hpp>
#include <synk/graphics/window/handlers/EventHandler.hpp>

#undef None

namespace synk::graphics::window
{
    class InputHandler : public synk::utils::utilities::NonCopyable
    {
        public:

            enum class InputStatus
            {
                None,
                Down,
                Up
            };

            InputHandler    ();
            ~InputHandler   ();

            void        update              ()    noexcept;
            void        keyRepeatEvent      (std::queue<EventHandler::Event>& queue)    const   noexcept;
            void        buttonRepeatEvent   (std::queue<EventHandler::Event>& queue)    const   noexcept;

            inline auto inputKeyboard           (std::queue<EventHandler::Event>& queue, const Keyboard::Key& key,  const InputStatus& status)   noexcept;
            inline auto inputMouse              (std::queue<EventHandler::Event>& queue, const Mouse::Button& button,  const InputStatus& status)   noexcept;

            inline auto isKeyPressed    (const Keyboard::Key& key)  const   noexcept;
            inline auto isKeyRepeat     (const Keyboard::Key& key)  const   noexcept;
            inline auto isKeyReleased   (const Keyboard::Key& key)  const   noexcept;

            inline auto isButtonPressed (const Mouse::Button& button) const   noexcept;
            inline auto isButtonRepeat  (const Mouse::Button& button) const   noexcept;
            inline auto isButtonReleased(const Mouse::Button& button) const   noexcept;

        private:

            std::array<InputStatus, static_cast<int> (Keyboard::Key::NumKeys) + 1>          m_event_keys;
            std::array<bool,        static_cast<int> (Keyboard::Key::NumKeys) + 1>          m_current_keys;
            std::array<bool,        static_cast<int> (Keyboard::Key::NumKeys) + 1>          m_down_keys;
            std::array<bool,        static_cast<int> (Keyboard::Key::NumKeys) + 1>          m_up_keys;

            std::array<InputStatus, static_cast<int> (Mouse::Button::NumButtons) + 1>       m_event_buttons;
            std::array<bool,        static_cast<int> (Mouse::Button::NumButtons) + 1>       m_current_buttons;
            std::array<bool,        static_cast<int> (Mouse::Button::NumButtons) + 1>       m_down_buttons;
            std::array<bool,        static_cast<int> (Mouse::Button::NumButtons) + 1>       m_up_buttons;

            inline auto keyEvent    (const Keyboard::Key& key, const InputStatus& status)  const   noexcept;
            inline auto buttonEvent (const Mouse::Button& button, const InputStatus& status)  const   noexcept;
    };
}

#include <synk/graphics/window/handlers/InputHandler.inl>
