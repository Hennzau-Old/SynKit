#pragma once

#include <iostream>
#include <functional>
#include <vector>
#include <map>

#include <synk/utils/utilities/NonCopyable.hpp>

#include <synk/graphics/window/handlers/Keys.hpp>
#include <synk/graphics/window/handlers/Buttons.hpp>

#undef None

namespace synk::graphics::window
{
    class EventHandler  :   public synk::utils::utilities::NonCopyable
    {
        public:

            enum class EventType
            {
                None,
                Closed,                 // Done
                Resized,                // Done
                Moved,                  // Done
                Minimized,              // Done on Win32
                Maximized,              // Done on Win32
                KeyPressed,             // Done
                KeyRepeat,              // Done
                KeyReleased,            // Done
                MouseButtonPressed,     // Done
                MouseButtonRepeat,      // Done
                MouseButtonReleased,    // Done
                MouseEntered,           // Done
                MouseExited,            // Done
                MouseMoved              // Done
            };

            struct Event
            {
                EventType    type = EventType::None;

                struct ResizedEvent
                {
                    std::uint16_t   width;
                    std::uint16_t   height;
                }   resized;

                struct MovedEvent
                {
                    std::int16_t    x;
                    std::int16_t    y;
                }   moved;

                struct MouseMovedEvent
                {
                    std::int16_t    x;
                    std::int16_t    y;
                }   mouse_moved;

                Keyboard::Key       key     =   Keyboard::Key::None;
                Mouse::Button       button  =   Mouse::Button::None;
            };

            using callback  =   std::function<void(const Event&)>;

            EventHandler    ()
            {}

            ~EventHandler   ()
            {}

            inline auto     add     (const EventType&   type,   const callback& function)   noexcept;
            inline auto     process (const Event& event) noexcept;

        private:

            std::map<EventType, std::vector<callback>>  m_callbacks;
    };
}

#include <synk/graphics/window/handlers/EventHandler.inl>
