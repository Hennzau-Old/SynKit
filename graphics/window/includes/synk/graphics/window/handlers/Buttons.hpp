#pragma once

#undef None

namespace synk::graphics::window
{
    namespace Mouse
    {
        enum class Button
        {
            None,
            Left,
            Middle,
            Right,
            NumButtons
        };
    }
}
