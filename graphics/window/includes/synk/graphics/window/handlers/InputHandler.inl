#pragma once

#include <synk/graphics/window/handlers/InputHandler.hpp>

namespace synk::graphics::window
{
    inline auto InputHandler::keyEvent(const Keyboard::Key &key, const InputStatus& status)  const   noexcept
    {
        auto    event   =   EventHandler::Event { EventHandler::EventType::None };
        event.key       =   key;

        if (status ==  InputStatus::Down)
        {
            event.type  =   EventHandler::EventType::KeyPressed;
        } else if (status == InputStatus::Up)
        {
            event.type  =   EventHandler::EventType::KeyReleased;
        }

        return event;
    }

    inline auto InputHandler::buttonEvent(const Mouse::Button &button, const InputStatus &status) const noexcept
    {
        auto    event   =   EventHandler::Event { EventHandler::EventType::None };
        event.button    =   button;

        if (status ==  InputStatus::Down)
        {
            event.type  =   EventHandler::EventType::MouseButtonPressed;
        } else if (status == InputStatus::Up)
        {
            event.type  =   EventHandler::EventType::MouseButtonReleased;
        }

        return event;
    }

    inline auto InputHandler::inputKeyboard(std::queue<EventHandler::Event>& queue, const Keyboard::Key &key, const InputStatus &status) noexcept
    {
        auto&   key_status  =   m_event_keys[static_cast<int> (key)];

        if (status == InputStatus::Down && key_status != status)
        {
            key_status  =   status;
            queue.push(keyEvent(key, status));
        } else if (status == InputStatus::Up)
        {
            key_status  =   status;
            queue.push(keyEvent(key, status));
        }
    }

    inline auto InputHandler::inputMouse(std::queue<EventHandler::Event> &queue, const Mouse::Button &button, const InputStatus &status)    noexcept
    {
        auto&   button_status   =   m_event_buttons[static_cast<int>(button)];

        if (status == InputStatus::Down && button_status != status)
        {
            button_status   =   status;
            queue.push(buttonEvent(button, status));
        } else if (status == InputStatus::Up)
        {
            button_status   =   status;
            queue.push(buttonEvent(button, status));
        }
    }

    inline auto InputHandler::isKeyPressed(const Keyboard::Key &key)    const   noexcept
    {
        return m_down_keys[static_cast<int>(key)];
    }

    inline auto InputHandler::isKeyRepeat(const Keyboard::Key &key) const   noexcept
    {
        return m_event_keys[static_cast<int>(key)]  ==  InputStatus::Down;
    }

    inline auto InputHandler::isKeyReleased(const Keyboard::Key &key)   const   noexcept
    {
        return m_up_keys[static_cast<int>(key)];
    }

    inline auto InputHandler::isButtonPressed(const Mouse::Button &button) const noexcept
    {
        return m_down_buttons[static_cast<int>(button)];
    }

    inline auto InputHandler::isButtonRepeat(const Mouse::Button &button) const noexcept
    {
        return m_event_buttons[static_cast<int>(button)]  ==  InputStatus::Down;
    }

    inline auto InputHandler::isButtonReleased(const Mouse::Button &button) const noexcept
    {
        return m_up_buttons[static_cast<int>(button)];
    }
}
