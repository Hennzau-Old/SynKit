#pragma once

#include <synk/graphics/window/AbstractedWindow.hpp>

#include <iostream>
#include <queue>

namespace synk::graphics::window
{
    inline auto AbstractedWindow::closeEvent()  noexcept
    {
        m_events.push({EventHandler::EventType::Closed});

        m_is_open   =   false;
    }

    inline auto AbstractedWindow::resizeEvent(const std::uint16_t &width, const std::uint16_t &height)  noexcept
    {
        const auto  event   =   EventHandler::Event
        {
            .type       =   EventHandler::EventType::Resized,
            .resized    =   {
                                .width  =   width,
                                .height =   height
                            }
        };

        m_settings.width    =   width;
        m_settings.height   =   height;

        m_events.push(event);
    }

    inline auto AbstractedWindow::moveEvent(const std::int16_t &x, const std::int16_t &y)   noexcept
    {
        auto    event   =   EventHandler::Event
        {
            .type   =   EventHandler::EventType::Moved
        };

        event.moved =   { x, y };

        m_settings.x    =   x;
        m_settings.y    =   y;

        m_events.push(event);
    }

    inline auto AbstractedWindow::minimizeEvent()   noexcept
    {
        m_events.push({ EventHandler::EventType::Minimized });
    }

    inline auto AbstractedWindow::maximizeEvent()   noexcept
    {
        m_events.push({ EventHandler::EventType::Maximized });
    }

    inline auto AbstractedWindow::keyPressedEvent(const Keyboard::Key &key) noexcept
    {
        m_input_handler->inputKeyboard(m_events, key, InputHandler::InputStatus::Down);
    }

    inline void AbstractedWindow::keyRepeatEvent()  noexcept
    {
        m_input_handler->keyRepeatEvent(m_events);
    }

    inline auto AbstractedWindow::keyReleasedEvent(const Keyboard::Key &key)    noexcept
    {
        m_input_handler->inputKeyboard(m_events, key, InputHandler::InputStatus::Up);
    }

    inline auto AbstractedWindow::buttonPressedEvent(const Mouse::Button &button)   noexcept
    {
        m_input_handler->inputMouse(m_events, button, InputHandler::InputStatus::Down);
    }

    inline void AbstractedWindow::buttonRepeatEvent()   noexcept
    {
        m_input_handler->buttonRepeatEvent(m_events);
    }

    inline auto AbstractedWindow::buttonReleasedEvent(const Mouse::Button &button)   noexcept
    {
        m_input_handler->inputMouse(m_events, button, InputHandler::InputStatus::Up);
    }

    inline auto AbstractedWindow::mouseMovedEvent(const std::int16_t &x, const std::int16_t &y) noexcept
    {
        auto    event   =   EventHandler::Event
        {
            .type   =   EventHandler::EventType::MouseMoved
        };

        event.mouse_moved =   { x, y };

        m_events.push(event);
    }

    inline auto AbstractedWindow::mouseEnteredEvent()   noexcept
    {
        m_events.push({ EventHandler::EventType::MouseEntered });
    }

    inline auto AbstractedWindow::mouseExitedEvent()    noexcept
    {
        m_events.push({ EventHandler::EventType::MouseExited });
    }
}
