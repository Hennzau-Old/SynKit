#pragma once

#include <synk/utils/utilities/Memory.hpp>

namespace synk::graphics::window
{
    class AbstractedWindow;
    DECLARE_PTR_AND_REF(AbstractedWindow)

    class SynkWindow;
    DECLARE_PTR_AND_REF(SynkWindow)
}
