cmake_minimum_required(VERSION 3.16)

project(
    SynKit_Graphics_Qt
    CXX
)

file(
    GLOB_RECURSE

    qt_headers

    includes/*.hpp
)

file(
    GLOB_RECURSE

    qt_inlines

    includes/*.inl
)

file(
    GLOB_RECURSE

    qt_sources

    src/*.cpp
)

add_library(
    SynKit_Graphics_Qt

    ${qt_headers}
    ${qt_inlines}
    ${qt_sources}
)

target_include_directories(
    SynKit_Graphics_Qt

    PUBLIC
    includes/
)

target_compile_features(
    SynKit_Graphics_Qt

    PUBLIC
    cxx_std_20
)

set_target_properties(
    SynKit_Graphics_Qt

    PROPERTIES
    LINKER_LANGUAGE
    CXX
)

if (SYNKIT_GRAPHICS_QT_EXAMPLES)

    file(
        GLOB_RECURSE

        qt_samples

        samples/*cpp
    )

    add_executable(
        SynKit_Graphics_Qt_Examples

        ${qt_samples}
    )

    target_link_libraries(
        SynKit_Graphics_Qt_Examples

        PRIVATE
        SynKit_Graphics_Qt
    )

endif()

