#include <synk/graphics/OpenGL/components/Shaders.hpp>

using namespace synk::utils;
using namespace synk::utils::logs;
using namespace synk::graphics::OpenGL;

Logger::Module LOG_MODULE = { "SHADERS" };

Shaders::Shaders(const std::string& vertex_file, const std::string& fragment_file)
{
    const   auto    makeShader  = [](const GLuint& ID, const std::string& code)
    {

        auto    result          = GL_FALSE ;
        auto    info_log_length = 0 ;

        const auto  code_ptr    = code.c_str();

        glShaderSource(ID, 1, &code_ptr, nullptr);
        glCompileShader(ID);

        glGetShaderiv(ID, GL_COMPILE_STATUS, &result);
        glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &info_log_length);

        if (info_log_length > 0)
        {
            auto    shader_error_message    =   std::string {};
            shader_error_message.resize(info_log_length + 1);

            glGetShaderInfoLog(ID, info_log_length, nullptr, shader_error_message.data());

            Logger::fLog(LOG_MODULE, "Compile Error : " + shader_error_message);
        }
    };

    const auto  vertex_ID   =   glCreateShader(GL_VERTEX_SHADER);
    const auto  fragment_ID =   glCreateShader(GL_FRAGMENT_SHADER);

    auto    vertex_str      =   std::string { "" };
    auto    fragment_str    =   std::string { "" };

    try
    {
        vertex_str      =   synk::utils::utilities::File::readFile(vertex_file);

    } catch (const std::runtime_error& error)
    {
        Logger::fLog(LOG_MODULE, error.what());
    }

    try
    {
        fragment_str    =   synk::utils::utilities::File::readFile(fragment_file);

    } catch (const std::runtime_error& error)
    {
        Logger::fLog(LOG_MODULE, error.what());
    }

    makeShader(vertex_ID,   vertex_str);
    makeShader(fragment_ID, fragment_str);

    m_ID    =   glCreateProgram();

    glAttachShader(m_ID, vertex_ID);
    glAttachShader(m_ID, fragment_ID);

    glLinkProgram(m_ID);

    auto    result          = GL_FALSE;
    auto    info_log_length = 0;

    glGetProgramiv(m_ID, GL_LINK_STATUS, &result);
    glGetProgramiv(m_ID, GL_INFO_LOG_LENGTH, &info_log_length);

     if (info_log_length > 0)
    {
        auto    shader_error_message    =   std::string {};
        shader_error_message.resize(info_log_length + 1);

        glGetShaderInfoLog(m_ID, info_log_length, nullptr, shader_error_message.data());

        Logger::fLog(LOG_MODULE, "Link Error :" + shader_error_message);
    }

    glDetachShader(m_ID, vertex_ID);
    glDetachShader(m_ID, fragment_ID);

    glDeleteShader(vertex_ID);
    glDeleteShader(fragment_ID);
}

Shaders::~Shaders()
{
    glDeleteProgram(m_ID);
}

