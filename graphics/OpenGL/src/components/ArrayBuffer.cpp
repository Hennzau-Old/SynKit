#include <synk/graphics/OpenGL/components/ArrayBuffer.hpp>

using namespace synk::graphics::OpenGL;

ArrayBuffer::ArrayBuffer()
{
    glGenVertexArrays(1, &m_ID);
}

ArrayBuffer::~ArrayBuffer()
{
    glDeleteVertexArrays(1, &m_ID);
}
