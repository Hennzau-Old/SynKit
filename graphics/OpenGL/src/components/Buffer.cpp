#include <synk/graphics/OpenGL/components/Buffer.hpp>

using namespace synk::graphics::OpenGL;

Buffer::Buffer(const GLsizeiptr& size, const GL_ENUM& target, const GL_ENUM& usage, const void* data)
:
    m_size      { size },
    m_target    { target },
    m_usage     { usage }
{
    glGenBuffers(1, &m_ID);

    bind();

        glBufferData(static_cast<GLenum> (m_target), size, data, static_cast<GLenum> (m_usage));

    unbind();
}

Buffer::~Buffer()
{
    glDeleteBuffers(1, &m_ID);
}
