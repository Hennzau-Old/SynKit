#include <synk/graphics/OpenGL/Module.hpp>
#include <cstring>

#ifdef SYNK_OS_WINDOWS

#include <synk/graphics/window/windows/Win32Window.hpp>
#include <GL/GL.h>

#pragma comment(lib, "opengl32.lib")

#elif SYNK_OS_MACOS

#elif SYNK_OS_LINUX

    #include <synk/graphics/window/windows/XlibWindow.hpp>

    #include <GL/gl.h>
    #include <GL/glx.h>

#endif

#include <synk/utils/logs/Logger.hpp>

#include <synk/graphics/window/SynkWindow.hpp>

#include <synk/graphics/OpenGL/components/Shaders.hpp>
#include <synk/graphics/OpenGL/components/Buffer.hpp>
#include <synk/graphics/OpenGL/components/ArrayBuffer.hpp>

using namespace synk;
using namespace synk::utils;
using namespace synk::utils::logs;
using namespace synk::graphics;
using namespace synk::graphics::window;
using namespace synk::graphics::OpenGL;

static inline constexpr const auto  OPENGL_MODULE   =   "OpenGL";

Module::Module(SynkWindow&    window)
:
    m_window    { &window }
{
    Logger::init({OPENGL_MODULE});

    #ifdef SYNK_OS_WINDOWS
    {
        Logger::iLog({ OPENGL_MODULE }, "Starting OpenGL context creation for Windows with Win32API");

        const auto* win32_window    = reinterpret_cast<const Win32Window*> (m_window->getAbstracted().get());

        m_window->build();

        auto  pixel_format_descriptor   = PIXELFORMATDESCRIPTOR
        {
            sizeof(PIXELFORMATDESCRIPTOR),
            1,
            PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    // Flags
            PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
            32,                   // Colordepth of the framebuffer.
            0, 0, 0, 0, 0, 0,
            0,
            0,
            0,
            0, 0, 0, 0,
            24,                   // Number of bits for the depthbuffer
            8,                    // Number of bits for the stencilbuffer
            0,                    // Number of Aux buffers in the framebuffer.
            PFD_MAIN_PLANE,
            0,
            0, 0, 0
        };

        const auto  format  =   ChoosePixelFormat(win32_window->getDC(), &pixel_format_descriptor);
        SetPixelFormat(win32_window->getDC(), format, &pixel_format_descriptor);

        const auto  active_format   =   GetPixelFormat(win32_window->getDC());

        DescribePixelFormat(win32_window->getDC(), active_format, sizeof(PIXELFORMATDESCRIPTOR), &pixel_format_descriptor);

        if ((pixel_format_descriptor.dwFlags & PFD_SUPPORT_OPENGL) != PFD_SUPPORT_OPENGL)
        {
            Logger::fLog({ OPENGL_MODULE }, "Device don't support OpenGL");
            return;
        }

        const auto  temp_context   =   wglCreateContext(win32_window->getDC());
        wglMakeCurrent(win32_window->getDC(), temp_context);

        typedef HGLRC (APIENTRY* PNFWGLCREATECONTEXTATTRIBSARBPROC) (HDC, int, int*);
        PNFWGLCREATECONTEXTATTRIBSARBPROC   wglCreateContextAttribsARB  =   0;

        wglCreateContextAttribsARB  =   reinterpret_cast<PNFWGLCREATECONTEXTATTRIBSARBPROC> (wglGetProcAddress("wglCreateContextAttribsARB"));

        int   attributes[]    =
        {
            0x2091,     4,      // MAJOR
            0x2092,     3,      // MINOR
            0x2094,     0,
            0x9126,
            0x00000001, 0
        };

        m_context   =   wglCreateContextAttribsARB(win32_window->getDC(), 0, attributes);

        wglDeleteContext(temp_context);
        wglMakeCurrent(win32_window->getDC(), m_context);
    }

    #elif SYNK_OS_LINUX
    {
        Logger::iLog({ OPENGL_MODULE }, "Starting OpenGL context creation for Linux with Xlib");

        auto    xlib_window =   reinterpret_cast<XlibWindow*>(m_window->getAbstracted().get());
        auto    display     =   xlib_window->getDisplay();
        auto    screen      =   xlib_window->getScreen();

        auto attributes = std::array<GLint, 23>
        {
                GLX_X_RENDERABLE    , True,
                GLX_DRAWABLE_TYPE   , GLX_WINDOW_BIT,
                GLX_RENDER_TYPE     , GLX_RGBA_BIT,
                GLX_X_VISUAL_TYPE   , GLX_TRUE_COLOR,
                GLX_RED_SIZE        , 8,
                GLX_GREEN_SIZE      , 8,
                GLX_BLUE_SIZE       , 8,
                GLX_ALPHA_SIZE      , 8,
                GLX_DEPTH_SIZE      , 24,
                GLX_STENCIL_SIZE    , 8,
                GLX_DOUBLEBUFFER    , True,
                0
        };

        auto    framebuffer_count   =   0;
        auto    framebuffer_config  =   glXChooseFBConfig(display, screen, attributes.data(), &framebuffer_count);

        if (!framebuffer_count)
        {
            Logger::fLog({OPENGL_MODULE}, "Failed to retrieve a framebuffer!");
        }

        auto    best_fbc = -1,
                worst_fbc = -1,
                best_num_samp = -1,
                worst_num_samp = 999;

        for (auto i = 0; i < framebuffer_count; ++i)
        {
            auto vi = glXGetVisualFromFBConfig( display, framebuffer_config[i] );

            if (vi != 0)
            {
                auto    samp_buf = 0,
                        samples = 0;

                glXGetFBConfigAttrib(display, framebuffer_config[i], GLX_SAMPLE_BUFFERS, &samp_buf);
                glXGetFBConfigAttrib(display, framebuffer_config[i], GLX_SAMPLES       , &samples );

                if (best_fbc < 0 || (samp_buf && samples > best_num_samp))
                {
                    best_fbc = i;
                    best_num_samp = samples;
                }

                if (worst_fbc < 0 || !samp_buf || samples < worst_num_samp)
                {
                    worst_fbc = i;
                }

                worst_num_samp = samples;
            }

            XFree(vi);
        }

        GLXFBConfig best_framebuffer = framebuffer_config[best_fbc];
        XFree(framebuffer_config);

        auto    visual  =   glXGetVisualFromFBConfig(display, best_framebuffer);
        if (!visual)
        {
            Logger::fLog({OPENGL_MODULE}, "Could not create a correct visual for the window");
        } else
        {
            xlib_window->getVisualInfo()    =   *visual;
        }

        if (screen != visual->screen)
        {
            Logger::fLog({OPENGL_MODULE}, "Screens doesn't match!");
        }

        XFree(visual);

        XSetWindowAttributes    window_attributes;
        window_attributes.border_pixel          = BlackPixel(display, screen);
        window_attributes.background_pixel      = WhitePixel(display, screen);
        window_attributes.override_redirect     = True;
        window_attributes.colormap              = XCreateColormap(display, RootWindow(display, screen), xlib_window->getVisualInfo().visual, AllocNone);
        window_attributes.event_mask            = ExposureMask;

        xlib_window->getAttributes()    =   window_attributes;

        m_window->build();


        auto glXCreateContextAttribsARB = reinterpret_cast<glXCreateContextAttribsARBProc> (glXGetProcAddressARB((const GLubyte *) "glXCreateContextAttribsARB"));

        auto context_attributes = std::array<int, 7>
        {
            GLX_CONTEXT_MAJOR_VERSION_ARB,  4,
            GLX_CONTEXT_MINOR_VERSION_ARB,  3,
            GLX_CONTEXT_FLAGS_ARB,          GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
            0
        };

        auto isExtensionSupported = [] (const char *extList, const char *extension) -> bool
        {
            return strstr(extList, extension) != 0;
        };

        const auto  extensions  =   glXQueryExtensionsString(display, screen);
        if (!isExtensionSupported(extensions, "GLX_ARB_create_context"))
        {
            Logger::wLog({OPENGL_MODULE}, "GLX_ARB_create_context not supported!");

            m_context   =   glXCreateNewContext(display, best_framebuffer, GLX_RGBA_TYPE, 0, True);
        } else
        {
            m_context   =   glXCreateContextAttribsARB(display, best_framebuffer, 0, true, context_attributes.data());
        }

        XSync(display, False);

        if (!glXIsDirect(display, m_context))
        {
            Logger::iLog({OPENGL_MODULE}, "Indirect GLX rendering context created!");
        } else
        {
            Logger::iLog({OPENGL_MODULE}, "Direct GLX rendering context created!");
        }

        glXMakeCurrent(display, xlib_window->getWindow(), m_context);
    }
    #endif

    if (!gladLoadGL())
    {
        Logger::fLog({OPENGL_MODULE}, "Cound not initialize OpenGL with GLAD loader");
    }

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    const auto  vendor      =  std::string { reinterpret_cast<const char*> (glGetString(GL_VENDOR)) };
    const auto  renderer    =  std::string { reinterpret_cast<const char*> (glGetString(GL_RENDERER)) };
    const auto  version     =  std::string { reinterpret_cast<const char*> (glGetString(GL_VERSION)) };

    Logger::iLog({OPENGL_MODULE}, "Informations ->");
    Logger::iLog({OPENGL_MODULE}, "Vendor           : " + vendor);
    Logger::iLog({OPENGL_MODULE}, "Renderer         : " + renderer);
    Logger::iLog({OPENGL_MODULE}, "Version          : " + version);
    Logger::exit({OPENGL_MODULE});

    setViewport({ 0.0f, 0.0f, static_cast<float> (m_window->getWidth()), static_cast<float> (m_window->getHeight()) });
}

Module::~Module()
{
    #ifdef SYNK_OS_WINDOWS

    wglDeleteContext(m_context);

    #elif SYNK_OS_LINUX

    const auto  xlib_window =   reinterpret_cast<const XlibWindow*>(m_window->getAbstracted().get());

    glXDestroyContext(xlib_window->getDisplay(), m_context);

    #endif
}

void Module::setClearColor(const utils::maths::Vec4<float> &color)  const   noexcept
{
    glClearColor(color.getX(), color.getY(), color.getZ(), color.getW());
}

void Module::setVSync(const bool &v)    const   noexcept
{
    #ifdef SYNK_OS_WINDOWS

    typedef BOOL (APIENTRY* PFNWGLSWAPINTERVALPROC) (int);
    PFNWGLSWAPINTERVALPROC  wglSwapInterval =   0;

    wglSwapInterval =   reinterpret_cast<PFNWGLSWAPINTERVALPROC> (wglGetProcAddress("wglSwapIntervalEXT"));

    if (wglSwapInterval)
    {
        wglSwapInterval(v);
        if (v)
        {
            Logger::iLog({OPENGL_MODULE}, "VSync set to : ON");
        } else
        {
            Logger::iLog({OPENGL_MODULE}, "VSync set to : OFF");
        }
    }

    #elif SYNK_OS_LINUX

    PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT = reinterpret_cast<PFNGLXSWAPINTERVALEXTPROC>(glXGetProcAddress(reinterpret_cast<const GLubyte*>("glXSwapIntervalEXT")));

    if (glXSwapIntervalEXT)
    {
        const auto  xlib_window =   reinterpret_cast<const XlibWindow*>(m_window->getAbstracted().get());

        glXSwapIntervalEXT(xlib_window->getDisplay(), xlib_window->getWindow(), v);
        if (v)
        {
            Logger::iLog({OPENGL_MODULE}, "VSync set to : ON");
        } else
        {
            Logger::iLog({OPENGL_MODULE}, "VSync set to : OFF");
        }
    }

    #endif
}

void Module::setViewport(const utils::maths::Vec4<float> &viewport) const   noexcept
{
    glViewport(viewport.getX(), viewport.getY(), viewport.getZ() - m_window->getBorderWidth(), viewport.getW() - m_window->getBorderHeight());
}

void Module::clearBuffers(const GL_ENUM &buffers)    const   noexcept
{
    glClear(static_cast<GLbitfield>(buffers));
}

void Module::swapBuffers()  const   noexcept
{
    #ifdef SYNK_OS_WINDOWS

    const auto* win32_window    = reinterpret_cast<const Win32Window*> (m_window->getAbstracted().get());

    SwapBuffers(win32_window->getDC());

    #elif SYNK_OS_LINUX

    const auto  xlib_window =   reinterpret_cast<const XlibWindow*>(m_window->getAbstracted().get());

    glXSwapBuffers(xlib_window->getDisplay(), xlib_window->getWindow());

    #endif
}
