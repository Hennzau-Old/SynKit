#pragma once

#include <synk/graphics/OpenGL/Module.hpp>

namespace synk::graphics::OpenGL
{
    inline auto Module::loadShaders(const std::string &file)    noexcept
    {
        if (m_shaders.find(file) == m_shaders.end())
        {
            m_shaders[file] =   std::make_unique<Shaders> (file + ".vert", file + ".frag");
        }

        return  synk::utils::utilities::makeConstObserver(m_shaders[file].get());
    }

    inline auto Module::createBuffer(const GLsizeiptr &size, const GL_ENUM &target, const GL_ENUM &usage, const void *data) const   noexcept
    {
        return std::make_unique<Buffer> (size, target, usage, data);
    }

    inline auto Module::createVertexBuffer(const GLsizeiptr &size, const GL_ENUM &usage, const void *data)  const   noexcept
    {
        return createBuffer(size, GL::ARRAY_BUFFER, usage, data);
    }

    inline auto Module::createIndexBuffer(const GLsizeiptr &size, const GL_ENUM &usage, const void *data)   const   noexcept
    {
        return createBuffer(size, GL::ELEMENT_ARRAY_BUFFER, usage, data);
    }

    inline auto Module::createArrayBuffer() const   noexcept
    {
        return std::make_unique<ArrayBuffer> ();
    }
}
