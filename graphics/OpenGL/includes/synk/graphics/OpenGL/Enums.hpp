#pragma once

#include <synk/utils/utilities/OS.hpp>

#include <synk/graphics/OpenGL/Loader.hpp>

namespace synk::graphics::OpenGL
{
    enum class GL_ENUM
    {
        COLOR_BUFFER_BIT    =   GL_COLOR_BUFFER_BIT,
        DEPTH_BUFFER_BIT    =   GL_DEPTH_BUFFER_BIT,

        ARRAY_BUFFER        =   GL_ARRAY_BUFFER,
        ELEMENT_ARRAY_BUFFER=   GL_ELEMENT_ARRAY_BUFFER,

        STATIC_DRAW         =   GL_STATIC_DRAW,
        DYNAMIC_DRAW        =   GL_DYNAMIC_DRAW,

        TRIANGLES           =   GL_TRIANGLES,

        FLOAT               =   GL_FLOAT,
        INT                 =   GL_INT,
        UNSIGNED_INT        =   GL_UNSIGNED_INT,
    };

    static GL_ENUM operator& (const GL_ENUM& s1, const GL_ENUM& s2)
    {
        return static_cast<GL_ENUM>(static_cast<int>(s1) & static_cast<int>(s2));
    }

    static GL_ENUM operator| (const GL_ENUM& s1, const GL_ENUM& s2)
    {
        return static_cast<GL_ENUM>(static_cast<int>(s1) | static_cast<int>(s2));
    }

    using GL    =   GL_ENUM;
}
