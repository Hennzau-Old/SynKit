#pragma once

#include <iostream>
#include <string>
#include <vector>

#include <synk/utils/utilities/NonCopyable.hpp>
#include <synk/utils/utilities/File.hpp>
#include <synk/utils/logs/Logger.hpp>

#include <synk/graphics/OpenGL/Loader.hpp>

namespace synk::graphics::OpenGL
{
    class Shaders : public synk::utils::utilities::NonCopyable
    {
        public:

            Shaders (const std::string& vertex_file,
                     const std::string& fragment_file);
            ~Shaders();

            inline auto bind        ()  const   noexcept;
            inline auto unbind      ()  const   noexcept;

            inline auto getID       ()  const   noexcept    { return m_ID; }

        private:

            GLuint              m_ID;
    };
}

#include <synk/graphics/OpenGL/components/Shaders.inl>
