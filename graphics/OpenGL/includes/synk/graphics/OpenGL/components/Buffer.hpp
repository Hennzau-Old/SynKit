#pragma once

#include <iostream>
#include <vector>

#include <synk/utils/utilities/NonCopyable.hpp>

#include <synk/graphics/OpenGL/Enums.hpp>

namespace synk::graphics::OpenGL
{
    class Buffer : public synk::utils::utilities::NonCopyable
    {
        public:

            Buffer  (const GLsizeiptr& size, const GL_ENUM& target,  const GL_ENUM& usage, const void* data = nullptr);
            ~Buffer ();

            inline auto bind    ()  const   noexcept;
            inline auto unbind  ()  const   noexcept;

            inline auto emplace (const GLintptr&    offset,
                                 const GLsizeiptr&  size,
                                 const void*        data)   const   noexcept;

            inline auto setAttribPointers   (const std::vector<GLint>& data_formats,
                                             const GL_ENUM&  type)   noexcept;

            inline auto         getID       ()  const   noexcept    { return m_ID; }

        private:

            GLuint          m_ID;

            GLsizeiptr      m_size;
            GL_ENUM         m_target;
            GL_ENUM         m_usage;

            struct VertexAttribs
            {
                std::uint32_t   offset  =   0;
                GLsizei         stride  =   0;
            }   m_vertex_attribs;
    };
}

#include <synk/graphics/OpenGL/components/Buffer.inl>
