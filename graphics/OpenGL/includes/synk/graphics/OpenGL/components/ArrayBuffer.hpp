#pragma once

#include <synk/utils/utilities/NonCopyable.hpp>

#include <synk/graphics/OpenGL/Loader.hpp>
#include <synk/graphics/OpenGL/components/Buffer.hpp>

namespace synk::graphics::OpenGL
{
    class ArrayBuffer
    {
        public:

            ArrayBuffer ();
            ~ArrayBuffer();

            inline auto     bind    ()  const   noexcept;
            inline auto     unbind  ()  const   noexcept;

            inline auto     draw    (const GL_ENUM&  mode,
                                     const GLint&   first,
                                     const GLsizei& count)  const   noexcept;

            inline auto     drawElements    (const GL_ENUM&  mode,
                                             const GLsizei& count,
                                             const GL_ENUM&  type,
                                             const Buffer&  index_buffer)   const   noexcept;

            inline auto     getID   ()  const   noexcept    { return m_ID; }

        private:

            GLuint          m_ID;
    };
}

#include <synk/graphics/OpenGL/components/ArrayBuffer.inl>
