#pragma once

#include <synk/graphics/OpenGL/components/Buffer.hpp>

#include <synk/utils/logs/Logger.hpp>

using namespace synk::utils::logs;

const Logger::Module    LOG_MODULE  =   { "BufferObject" };

namespace synk::graphics::OpenGL
{
    inline auto Buffer::bind()  const   noexcept
    {
        glBindBuffer(static_cast<GLenum> (m_target), m_ID);
    }

    inline auto Buffer::unbind()    const   noexcept
    {
        glBindBuffer(static_cast<GLenum> (m_target), 0);
    }

    inline auto Buffer::emplace(const GLintptr &offset, const GLsizeiptr &size, const void *data)   const   noexcept
    {
        bind();

            glBufferSubData(static_cast<GLenum> (m_target), offset, size, data);

        unbind();
    }

    inline auto Buffer::setAttribPointers(const std::vector<GLint> &data_formats, const GL_ENUM &type) noexcept
    {
        bind();

            auto    size    =   0.0f;

            switch (type)
            {
                case GL::FLOAT:         size    =   sizeof(float);          break;
                case GL::INT:           size    =   sizeof(int);            break;
                case GL::UNSIGNED_INT:  size    =   sizeof(unsigned int);   break;
                default:
                {
                    Logger::fLog(LOG_MODULE, "No type recognize in setAttribPointers function !");
                }
            }

            for (const auto& data_size  :   data_formats)
            {
                m_vertex_attribs.stride +=  data_size * size;
            }

            for (auto i = 0u; i < data_formats.size(); i++)
            {
                glEnableVertexAttribArray(i);
                glVertexAttribPointer(i, data_formats[i], static_cast<GLenum> (type), GL_FALSE, m_vertex_attribs.stride, reinterpret_cast<const void*>(m_vertex_attribs.offset));

                m_vertex_attribs.offset +=  data_formats[i] *   size;
            }

        unbind();
    }
}
