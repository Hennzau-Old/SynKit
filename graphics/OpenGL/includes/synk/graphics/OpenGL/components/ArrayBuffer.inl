#pragma once

#include <synk/graphics/OpenGL/components/ArrayBuffer.hpp>

namespace synk::graphics::OpenGL
{
    inline auto ArrayBuffer::bind()  const   noexcept
    {
        glBindVertexArray(m_ID);
    }

    inline auto ArrayBuffer::unbind()    const   noexcept
    {
        glBindVertexArray(0);
    }

    inline auto ArrayBuffer::draw(const GL_ENUM &mode, const GLint &first, const GLsizei &count) const   noexcept
    {
        bind();

            glDrawArrays(static_cast<GLenum> (mode), first, count);

        unbind();
    }

    inline auto ArrayBuffer::drawElements(const GL_ENUM &mode, const GLsizei &count, const GL_ENUM &type, const Buffer& index_buffer)    const   noexcept
    {
        bind();

            index_buffer.bind();

                glDrawElements(static_cast<GLenum> (mode), count, static_cast<GLenum> (type), nullptr);

            index_buffer.unbind();

        unbind();
    }
}
