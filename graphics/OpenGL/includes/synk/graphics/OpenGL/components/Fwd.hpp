#pragma once

#include <synk/utils/utilities/Memory.hpp>

namespace synk::graphics::OpenGL
{
    class Shaders;
    DECLARE_PTR_AND_REF(Shaders)

    class Buffer;
    DECLARE_PTR_AND_REF(Buffer)

    class ArrayBuffer;
    DECLARE_PTR_AND_REF(ArrayBuffer)
}
