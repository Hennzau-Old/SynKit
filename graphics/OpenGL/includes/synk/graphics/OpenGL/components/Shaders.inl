#pragma once

#include <synk/graphics/OpenGL/components/Shaders.hpp>

namespace synk::graphics::OpenGL
{
    inline auto Shaders::bind() const   noexcept
    {
        glUseProgram(m_ID);
    }

    inline auto Shaders::unbind()   const   noexcept
    {
        glUseProgram(0);
    }
}
