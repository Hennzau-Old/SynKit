#pragma once

#include <iostream>
#include <map>

#include <synk/graphics/OpenGL/Loader.hpp>

#include <synk/utils/utilities/NonCopyable.hpp>
#include <synk/utils/utilities/OS.hpp>
#include <synk/utils/maths/Maths.hpp>

#ifdef SYNK_OS_WINDOWS

#include <windows.h>

#elif SYNK_OS_LINUX

#include <GL/glx.h>

#endif

#include <synk/graphics/window/Fwd.hpp>

#include <synk/graphics/OpenGL/Enums.hpp>
#include <synk/graphics/OpenGL/components/Fwd.hpp>
#include <synk/graphics/OpenGL/components/ArrayBuffer.hpp>
#include <synk/graphics/OpenGL/components/Shaders.hpp>

namespace synk::graphics::OpenGL
{
    class Module : public synk::utils::utilities::NonCopyable
    {
        public:

            Module  (window::SynkWindow& window);
            ~Module ();

            void        setClearColor   (const utils::maths::Vec4<float>& color)    const   noexcept;
            void        setVSync        (const bool& v)                             const   noexcept;
            void        setViewport     (const utils::maths::Vec4<float>& viewport) const   noexcept;
            void        clearBuffers    (const GL_ENUM& buffers)                    const   noexcept;
            void        swapBuffers     ()                                          const   noexcept;

            inline auto loadShaders     (const std::string& file)                           noexcept;

            inline auto createBuffer        (const GLsizeiptr& size,
                                             const GL_ENUM& target,
                                             const GL_ENUM& usage,
                                             const void* data = nullptr)    const   noexcept;

            inline auto createVertexBuffer  (const GLsizeiptr& size,
                                             const GL_ENUM& usage,
                                             const void* data = nullptr)    const   noexcept;

            inline auto createIndexBuffer   (const GLsizeiptr& size,
                                             const GL_ENUM& usage,
                                             const void* data = nullptr)    const   noexcept;

            inline auto createArrayBuffer   ()  const   noexcept;

        private:

            window::SynkWindowObserverPtr  m_window;

            std::map<   const std::string,
                        ShadersOwnedPtr>    m_shaders;

        #ifdef SYNK_OS_WINDOWS
            HGLRC                           m_context;
        #elif SYNK_OS_LINUX

            typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

            GLXContext                      m_context;
        #endif
    };
}

#include <synk/graphics/OpenGL/Module.inl>
