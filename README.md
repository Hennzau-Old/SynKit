SynKit is a collaborative ToolKit in C++ completely "detachable". You cas use a specific sub-module or the full kit.
It's based on Synk Engine developped by Hennzau but with new ideas introduce by Seynax

**How to add :** 

SynKit is a simple CMake Library for C++20. You just have to clone the repository and *add_subdirectory* the clone on your PC.
Next juste specify the modules or sub-modules you want and *target_link_libraries* the *SynKit* target.

Here is an example : 

`cmake_minimum_required(VERSION 3.16)`

`project(ReadMeExample CXX)`

`set(SYNKIT_CORE_ENTITIES ON)   # enable a sub-module`

`set(SYNKIT_CORE ON)            # enable a module (all sub-modules)`

`set(SYNKIT_UTILS ON)`

`set(SYNKIT_GRAPHICS ON)`

`add_subdirectory(libs/SynKit)`

`add_executable(example main.cpp)`

`target_link_libraries(example SynKit)`
`

**How to build :**


The notable compatible IDEs on which the library was test are
- QTCreator
- Visual Studio
- Code::blocks
- Eclipse C++

On **Windows** you need MSVC or an other compiler which support C++20, threads etc...
juste cmake your project :
`mkdir build && cd build`

`cmake ..`

`cmake --build . --config Debug` #Or "Release"

On **Linux**, just `cmake -G "Unix Makefiles"` and `make` the target. 

**Library specification**

No dependencies are needed to use SynKit. Just the VulkanSDK if you want to use the Vulkan graphics sub-module.
(You can disable Vulkan submodule by using : `set(SYNKIT_GRAPHICS_VULKAN OFF`) in your CMakeLists.txt before the `add_subdirectory`

The *Window* graphics sub-module uses the Win32 API on Windows and Xcb or Xlib on Linux. On linux, the default lib is Xlib but you can use Xcb by using `set(SYNKIT_WINDOW_USE_XCB)`. 
**Warning** : You can't use the OpenGL graphics sub-module without using Xlib.

**Architecture :**

-   utils/              **using : independants submodules**
    -   utilities/
        -   includes/
        -   src/
        -   samples/
    -   maths/
        -   includes/
        -   src/
        -   samples/
    -   logs/
        -   includes/
        -   src/
        -   samples/
-   core/               **using : utils + himself**
    -   entities/
        -   includes/
        -   src/
        -   samples/
    -   physics/
        -   includes/
        -   src/
        -   samples/
    -   threads/
        -   includes/
        -   src/
        -   samples/
    -   network/
        -   includes/
        -   src/
        -   samples/
-   graphics/           **using : utils + core + himself**
    -   window/
        -   includes/
        -   src/
        -   samples/
    -   drawing_chain_system/
        -   includes/
        -   src/
        -   samples/
    -   Vulkan/
        -   includes/
        -   src/
        -   samples/
    -   OpenGL/
        -   includes/
        -   src/
        -   samples/
    -   Qt/
        -   includes/
        -   src/
        -   samples/
-   technic/            **using : utils + core + graphics + himself**
    -   communication_with_chained_drawing_chain_system/
        -   src/
        -   includes/
        -   samples/
    -   others...
        -   src/
        -   includes/
        -   samples/
        
***Features :***

    Maths :
    
        Vec1 to Vec6 + undefined size Vec
        Matrices
        Quaternion
    
    Physics :
        
        RigidBody : 2D and 3D collisions
        With ECS
    
    Threads :
    
        Made Threads easy
        Threads Manager
        Threads Synchronizer
        
    Network :
    
        TCP/UDP/FTP/...
        Compression / Encryption
        Priority system
        
    Graphics :
    
        OpenGL
        Vulkan
        QT
    
    IA :
    
        PathFinding
        
    Utilities :
    
        Greedy Meshing algorithm
        Node : 
            - Liste
            - Stack
            - Group
            - Tree / Graph
        