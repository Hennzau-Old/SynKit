#include <synk/core/entities/EntityManager.hpp>

using namespace synk::core::entities;

#include <synk/utils/logs/Logger.hpp>

EntityManager::EntityManager()
{
}

EntityManager::~EntityManager()
{

}

void EntityManager::update() noexcept
{
    for (auto& update : m_updates)
    {
        update(m_registry);
    }
}
