#pragma once

#include <synk/core/entities/EntityManager.hpp>

namespace synk::core::entities
{
    inline auto EntityManager::make()  noexcept
    {
        return m_registry.create();
    }

    template <typename... T>
    inline auto EntityManager::remove(entt::entity entity) noexcept
    {
        m_registry.remove<T...> (entity);
    }

    inline auto EntityManager::removeAll(entt::entity entity)   noexcept
    {
        m_registry.remove_all(entity);
    }

    inline auto EntityManager::destroy(entt::entity entity) noexcept
    {
        m_registry.destroy(entity);
    }

    template<typename T, typename...Args>
    inline void EntityManager::addComponent(entt::entity entity, Args&&... args) noexcept
    {
        m_registry.emplace<T>(entity, args...);
    }

    template<typename T>
    inline auto& EntityManager::get(entt::entity entity) const noexcept
    {
        return m_registry.get<T>(entity);
    }

    template<typename T>
    inline auto& EntityManager::get(entt::entity entity) noexcept
    {
        return m_registry.get<T>(entity);
    }

    template<typename... T>
    inline auto EntityManager::view() noexcept
    {
        return m_registry.view<T...>();
    }

    inline void EntityManager::addUpdateSystem(const std::function<void(entt::registry&)>& function) noexcept
    {
        m_updates.emplace_back(function);
    }
}
