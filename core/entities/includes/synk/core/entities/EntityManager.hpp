#pragma once

#undef max
#undef min

#include <entt/entt.hpp>

namespace synk::core::entities
{
    class EntityManager
    {
        public:

            EntityManager   ();
            ~EntityManager  ();

            void            update      ()  noexcept;
            void            render      ()  noexcept;

            template<typename T, typename... Args>
            inline void     addComponent(entt::entity entity, Args&&... args)   noexcept;

            inline auto     make        ()  noexcept;

            template<typename... T>
            inline auto     remove      (entt::entity)  noexcept;

            inline auto     removeAll   (entt::entity entity)  noexcept;
            inline auto     destroy     (entt::entity entity)  noexcept;

            template<typename T>
            inline auto&    get(entt::entity entity) const noexcept;

            template<typename T>
            inline auto&    get(entt::entity entity) noexcept;

            template<typename... T>
            inline auto     view() noexcept;

            inline void     addUpdateSystem (const std::function<void(entt::registry&)>& function) noexcept;

            inline auto&    getRegistry ()          noexcept    { return m_registry; }
            inline auto&    getRegistry ()  const   noexcept    { return m_registry; }

        private:

            entt::registry                                          m_registry;
            std::vector<std::function<void(entt::registry&)>>       m_updates;
    };
}

#include <synk/core/entities/EntityManager.inl>
