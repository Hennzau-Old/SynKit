#include <iostream>

#include <synk/core/entities/EntityManager.hpp>

int main(void)
{
    using namespace synk::core::entities;

    EntityManager   manager;

    const auto  entity_one  =   manager.make();
    const auto  entity_two  =   manager.make();
    const auto  entity_three=   manager.make();
    const auto  entity_four =   manager.make();

    struct Position
    {
        float x;
        float y;
        float z;
    };

    manager.addComponent<Position> (entity_one);
    manager.addComponent<Position> (entity_two);
    manager.addComponent<Position> (entity_three);
    manager.addComponent<Position> (entity_four);

    manager.view<Position>().each([](auto& pos)
    {
        pos.x   +=  5;
    });
}
