#include <iostream>

#include <synk/core/threads/ThreadPool.hpp>

int main(void)
{
    auto    pool    =   std::make_unique<synk::core::threads::ThreadPool> (5);
    auto    results =   std::vector<std::future<int>> ();

    for (auto i = 0; i < 10; i++)
    {
      results.push_back(pool->exec([](const int& j) -> int
      {
          std::cout << j << std::endl;

          return j;
      }, i));
    }

    pool->join();

    std::cout << "Answers : " << std::endl;

    for (auto&  result : results)
    {
      std::cout << result.get() << std::endl;
    }
}
