#pragma once

#include <synk/core/threads/ThreadPool.hpp>

#include <iostream>
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <future>
#include <functional>

namespace synk::core::threads
{
    template<class F, class... Args>
    inline  auto    ThreadPool::exec    (F&& f, Args&&... args) noexcept    -> std::future<typename std::invoke_result<F, Args...>::type>
    {
        using   return_type = typename std::invoke_result<F, Args...>::type;


        const   auto    task    {   std::make_shared<std::packaged_task<return_type()>>
                                (std::bind(std::forward<F>(f), std::forward<Args>(args)...)) };

        auto            result  { task->get_future() };

        m_tasks.push( [task]()
        {
            (*task) ();
        });

        return result;
    }

    inline void ThreadPool::join() noexcept
    {
        m_status    =   true;

        for (const auto& worker : m_workers)
        {
            worker->join();
        }
    }
}
