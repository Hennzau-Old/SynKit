#pragma once

#include <iostream>
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <future>
#include <functional>

namespace synk::core::threads
{
    class ThreadPool
    {
        public:
        
            ThreadPool  (const std::uint32_t& count);
            ~ThreadPool ();

            template<class F, class... Args>
            inline  auto    exec    (F&& f, Args&&... args) noexcept    -> std::future<typename std::invoke_result<F, Args...>::type>;
            inline  void    join    ()  noexcept;

            inline  auto&   getWorkers  ()          noexcept    { return m_workers; }
            inline  auto&   getWorkers  ()  const   noexcept    { return m_workers; }

            inline  auto&   getTasks    ()          noexcept    { return m_tasks; }
            inline  auto&   getTasks    ()  const   noexcept    { return m_tasks; }

            inline  auto&   getMutex    ()          noexcept    { return m_mutex; }
            inline  auto&   getMutex    ()  const   noexcept    { return m_mutex; }

        private:

            std::uint32_t                               m_count;
            std::vector<std::unique_ptr<std::thread>>   m_workers;
            std::unique_ptr<std::mutex>                 m_mutex;
            std::queue<std::function<void()>>           m_tasks;
            bool                                        m_status;
    };
};

#include <synk/core/threads/ThreadPool.inl>


